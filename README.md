# What is FoodBrain?

FoodBrain is a database for storing food related information. It can be used to make a list of your food sources, trace the origins of fresh foods and connect with (other) food producers.

The purpose of this is to create a resilient local food network, based around communities and small-scale food production instead of supermarkets and the globalised, just-in-time food industry.

This aligns in many ways with permaculture principles. Food sources for example, can be divided in zones, based on their accessibility. And the platform is not designed to make food producers compete for lowest price, but to build up long-term relationships with local consumers instead.

## Use cases

The first application of FoodBrain is for a project that maps food forests in the Philippines. This is why the primary object for storing data is called foodForest, but technically it could represent other farm types as well.

See https://philippinefoodforest.org for more information.

## Dependencies

### MODX

This software is distributed as an extra for MODX, a content management framework based on PHP and MySQL. Although the database model could in theory be used in other MySQL-based applications (with Directus, for example), for now it relies on MODX for providing the interface parts.

You need to have at least the following extras installed in MODX:

- pdoTools
- MIGX
- ImagePlus
- pThumb
- SuperBoxSelect

The following paid extras are optional, but recommended:

- Agenda (for managing food related events)
- ContentBlocks (for creating responsive web pages)
- Redactor (the rich text editor of choice)

### Semantic UI

A front-end framework called Semantic UI is used for displaying FoodBrain elements on a website. You can of course choose to roll out your own HTML/CSS, but with Semantic UI this will be plug and (dis)play.

>I strongly recommend installing [Fomantic UI](https://fomantic-ui.com), a community fork of Semantic UI, instead of the main repository. Development has stagnated there for some time now and Fomantic UI includes many important fixes that make working with it a lot less cumbersome. Comes with a few new components too.

If it's not your intention to display your data on a website, then you don't need to worry about any HTML/CSS. The interface for managing data is provided by MODX and MIGX.

### Romanesco

If you _are_ planning to use the data on a website, then you might want to take a look at Romanesco. Romanesco is a collection of tools for prototyping and building websites. It integrates a front-end pattern library (using Semantic UI) directly into the CMS (MODX).

So if you install Romanesco, everything you need to use FoodBrain is already included. Romanesco is also open source and free to download and use.

See https://romanesco.info for more information.

### License

FoodBrain is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

[GNU General Public License v3](https://gitlab.com/fractal-farming/foodbrain/blob/master/core/components/foodbrain/docs/license.md)