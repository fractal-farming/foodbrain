<?php

// Collections
// =====================================================================

$_lang['collections.forest.children'] = 'Forests';
$_lang['collections.forest.children.create'] = 'Add forest';
$_lang['collections.forest.children.back_to_collection_label'] = 'Back to overview';

$_lang['collections.component.children'] = 'Components';
$_lang['collections.component.children.create'] = 'Add component';
$_lang['collections.component.children.back_to_collection_label'] = 'Back to overview';
