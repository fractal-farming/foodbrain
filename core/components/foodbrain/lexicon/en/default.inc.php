<?php

// Map
// =====================================================================

$_lang['foodbrain.map.button_read_more'] = "More info";
$_lang['foodbrain.map.location_obfuscated_heading'] = "This is not the exact location.";
$_lang['foodbrain.map.location_obfuscated_content'] = "For privacy / security reasons, the marker has been placed randomly within a [[+radius]] km radius.";

// Forest
// =====================================================================

$_lang['foodbrain.forest.heading'] = "Forest";
$_lang['foodbrain.forest.trees'] = "Trees";
$_lang['foodbrain.forest.contact'] = "Guardian";
$_lang['foodbrain.forest.maintainer'] = "Steward";

$_lang['foodbrain.forest.data_heading'] = "Forest details";
$_lang['foodbrain.forest.data_category'] = "Category";
$_lang['foodbrain.forest.data_size'] = "Land area";
$_lang['foodbrain.forest.data_age'] = "Age";
$_lang['foodbrain.forest.data_access'] = "Access";
$_lang['foodbrain.forest.data_website'] = "Website";
$_lang['foodbrain.forest.data_facebook'] = "Facebook page";

// Location
// =====================================================================

$_lang['foodbrain.location.heading'] = "Location";
$_lang['foodbrain.location.elevation'] = "Elevation";

// Source
// =====================================================================

$_lang['foodbrain.source.heading'] = "Source";
$_lang['foodbrain.source.unknown'] = "Unknown / Nature";

// Address
// =====================================================================

$_lang['foodbrain.address.heading'] = "Address";

// Component
// =====================================================================

$_lang['foodbrain.component.heading'] = "Component";
$_lang['foodbrain.component.maintainer'] = "Maintainer";

$_lang['foodbrain.component.data_zone'] = "Zone";
$_lang['foodbrain.component.data_type'] = "Type";
$_lang['foodbrain.component.data_stage'] = "Stage";

$_lang['foodbrain.component.inputs_heading'] = "Inputs";
$_lang['foodbrain.component.input_heading'] = "Input";
$_lang['foodbrain.component.input_name'] = "Name";
$_lang['foodbrain.component.input_type'] = "Type";
$_lang['foodbrain.component.input_internal'] = "Output of another component";
$_lang['foodbrain.component.input_external'] = "External source";

$_lang['foodbrain.component.outputs_heading'] = "Outputs";
$_lang['foodbrain.component.output_heading'] = "Output";
$_lang['foodbrain.component.output_name'] = "Name";
$_lang['foodbrain.component.output_type'] = "Type";
$_lang['foodbrain.component.output_resilience'] = "Resilience";
$_lang['foodbrain.component.output_quality'] = "Quality";
$_lang['foodbrain.component.output_availability'] = "Availability";

