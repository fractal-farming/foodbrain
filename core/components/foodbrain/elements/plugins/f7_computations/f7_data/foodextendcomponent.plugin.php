<?php
/**
 * FoodExtendComponent
 *
 * Generate row for component resource in the foodbrain_forest_components table.
 *
 * @var modX $modx
 */

switch ($modx->event->name) {
    case 'OnDocFormSave':
        /**
         * @var modResource $resource
         * @var int $id
         */

        // Abort if resource is not FoodForestComponent
        $templateID = $resource->get('template');
        if ($templateID != $modx->getOption('foodbrain.component_template_id')) {
            break;
        }

        // Get forest page from context settings
        $forestSetting = $modx->getObject('modContextSetting', array(
            'context_key' => $resource->get('context_key'),
            'key' => 'foodbrain.forest_properties_id'
        ));

        if (!is_object($forestSetting)) break;

        // Check if component already exists in table
        $component = $modx->getObject('foodForestComponent', array(
            'resource_id' => $id,
        ));

        // Create new component or update existing
        if (!is_object($component)) {
            $component = $modx->newObject('foodForestComponent',array(
                'resource_id' => $id,
                'forest_id' => $forestSetting->get('value'),
                'createdon' => time(),
                'createdby' => $modx->user->get('id'),
            ));
        } else {
            $component->set('forest_id', $forestSetting->get('value'));
        }

        $component->save();

        break;
}

return true;