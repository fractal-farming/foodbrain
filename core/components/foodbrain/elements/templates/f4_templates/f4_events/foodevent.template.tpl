<!DOCTYPE html>
<html id="[[*context_key]]" lang="[[++cultureKey]]">

[[[[If?
    &subject=`[[$headTheme]]`
    &operator=`isnull`
    &then=`$head`
    &else=`$headTheme`
]]]]

<body id="[[*alias]]" class="overview event">

<div class="pusher">
    <header id="header" class="masthead without hero [[++navbar_sticky:eq=`0`:then=`non-stick`]]">
        [[[[If?
            &subject=`[[$mainNavTheme]]`
            &operator=`isnull`
            &then=`$mainNav`
            &else=`$mainNavTheme`
        ]]]]
    </header>

    <main id="main" role="main">
        <article id="content">
            [[!AgendaDetail?
                &tpl=`foodEventDetail`
                &daterangeFormat=`||||g|:i a|`
                &categoryTpl=`foodEventRowCategory`
                &locationTpl=`foodEventRowLocation`
            ]]
        </article>
    </main>

    [[[[If?
        &subject=`[[$footerTheme]]`
        &operator=`isnull`
        &then=`$footer`
        &else=`$footerTheme`
    ]]]]
</div>

[[$offCanvasNav]]
[[$script]]

</body>
</html>