<!DOCTYPE html>
<html id="[[*context_key]]" lang="[[++cultureKey]]">

<head>
    [[[[modifiedIf?
        &subject=`headTheme`
        &operator=`iselement`
        &operand=`chunk`
        &then=`$headTheme`
        &else=`$head`
    ]]]]
</head>

<body id="[[*alias]]" class="vertical">

[[$offCanvasNav]]

<div class="pusher">
    <header id="header" class="[[++navbar_sticky:eq=`1`:then=`ui sticky`]] inverted primary-color">
        [[[[modifiedIf?
            &subject=`masthead`
            &operator=`iselement`
            &operand=`chunk`
            &then=`$masthead`
        ]]]]

        <nav id="menu-vertical" class="ui [[++navbar_size:default=`large`]] secondary vertical [[++navbar_level:lte=`1`:then=`inverted`:else=`accordion`]] menu" role="navigation">
            <div class="item branding">
                <a class="brand logo inverted" href="[[~125]]" title="[[%romanesco.menu.logo_title]]">[[++site_name]]</a>
            </div>

            <ul id="menu-accordion">
                [[!pdoMenu?
                    &startId=`125`
                    &level=`[[+level:empty=`[[++navbar_level]]`]]`
                    &levelClass=`level-`
                    &selfClass=`current`
                    &resources=`[[+excluded_resources]]`

                    &tplOuter=`navWrapper`
                    &tpl=`navItemAccordionParent`
                    &tplParentRow=`navItemAccordionParent`
                    &tplParentRowActive=`navItemAccordionParent@Active`
                    &tplInnerRow=`navItemAccordion`

                    &checkPermissions=`list`
                    &cache=`1`
                    &cache_key=`[[!+modx.user.id:memberof=`[[++romanesco.member_groups_frontend]]`:then=`nav_[[*context_key]]_member`:else=`nav_[[*context_key]]_anonymous`]]`
                ]]
            </ul>

            [[-$mainNavItemsTheme]]

            <div class="item toc">
                <button class="ui right labeled inverted basic icon button">
                    <i class="sidebar icon"></i>
                    <span>[[%romanesco.menu.toc]]</span>
                </button>
            </div>
        </nav>

        [[[[If?
            &subject=`[[+hero]]`
            &operator=`EQ`
            &operand=`1`
            &then=`$heroVertical`
        ]]]]

        [[[[If?
            &subject=`[[++search.add_to_menu]]`
            &operator=`EQ`
            &operand=`1`
            &then=`$searchForm`
        ]]]]
    </header>

    <main id="main">
        [[[[modifiedIf?
            &subject=`toolbarBasicTheme`
            &operator=`iselement`
            &operand=`chunk`
            &then=`$toolbarBasicTheme`
            &else=`$toolbarBasic`
        ]]]]

        <article id="content">
            [[$content]]
        </article>

        [[[[modifiedIf?
            &subject=`footerTheme`
            &operator=`iselement`
            &operand=`chunk`
            &then=`$footerTheme`
            &else=`$footer`
        ]]]]
    </main>
</div>

[[$script]]

</body>
</html>