<!DOCTYPE html>
<html id="[[*context_key]]" lang="[[++cultureKey]]">

<head>
    [[[[modifiedIf?
        &subject=`headTheme`
        &operator=`iselement`
        &operand=`chunk`
        &then=`$headTheme`
        &else=`$head`
    ]]]]
</head>

[[migxLoopCollection?
    &packageName=`foodbrain`
    &classname=`foodForest`
    &where=`[{"resource_id":"[[*forest_global_id:empty=`[[*id]]`]]"}]`
    &joins=`[{"alias":"Address"},{"alias":"Location"}]`
    &toJsonPlaceholder=`forestData`
]]
[[migxJsonToPlaceholders?
    &value=`[[+forestData]]`
    &prefix=`data.`
]]

[[migxLoopCollection?
    &packageName=`foodbrain`
    &classname=`foodForestPlant`
    &joins=`[{"alias":"Taxonomy"}]`
    &tpl=`foodForestPlantLabel`
    &where=`[{"forest_id":"[[*forest_global_id:empty=`[[*id]]`]]"},{"published":1}]`
    &sortConfig=`[{"sortby":"name","sortdir":"ASC"}]`
    &toPlaceholder=`forest_plants`
]]

<body id="[[*alias]]" class="overview forest">

[[$offCanvasNav]]

<div class="pusher">
    <header id="header" class="masthead [[++navbar_sticky:eq=`0`:then=`non-stick`]]">
        [[[[If?
            &subject=`[[$mainNavTheme]]`
            &operator=`isnull`
            &then=`$mainNav`
            &else=`$mainNavTheme`
        ]]]]

        <div id="hero" class="ui vertical stripe segment background [[*alias]]">
            <div class="ui grid container">
                <div class="sixteen wide mobile twelve wide tablet ten wide computer column">
                    <h1 class="ui inverted header">[[*pagetitle]]</h1>
                </div>
            </div>
        </div>
    </header>

    <main id="main" role="main">
        <article id="content">
            <div class="ui vertical stripe segment white">
                <div class="ui container">
                    <div class="ui stackable on tablet very relaxed equal width grid">
                        <div class="ten wide column">
                            <section id="forest-intro">
                                [[*introtext:replace=`<p>==<p class="lead">`]]
                            </section>

                            [[+forest_plants:notempty=`
                            <div class="ui section divider"></div>

                            <section id="forest-plants">
                                <div class="ui labels">
                                    [[+forest_plants]]
                                </div>
                            </section>
                            `]]
                        </div>
                        <div class="column">
                            <aside id="forest-connect">
                                [[+data.website:notempty=`<a href="[[+data.website]]" class="ui basic massive circular icon button" target="_blank" title="Visit website"><i class="globe icon"></i></a>`]]
                                [[+data.facebook:notempty=`<a href="[[+data.facebook]]" class="ui basic massive circular icon button" target="_blank" title="Visit Facebook page"><i class="facebook f icon"></i></a>`]]
                                [[+data.email:notempty=`<a href="mailto:[[+data.email]]" class="ui basic massive circular icon button" target="_blank" title="Send an email"><i class="envelope icon"></i></a>`]]
                            </aside>
                        </div>
                    </div>
                </div>
            </div>

            [[*content]]

            <div class="ui vertical stripe segment inverted primary-color">
                <div class="ui container">
                    <div class="ui stackable on tablet very relaxed equal width grid">

                        <div id="forest-details" class="seven wide computer six wide large screen column">
                            <h2 class="ui small header">[[%foodbrain.forest.data_heading]]</h2>

                            <table class="ui large compact very basic unstackable inverted table">
                                <tr>
                                    <td>[[%foodbrain.forest.data_category]]</td>
                                    <td>[[+data.category:renderInputOption=`alias`]]</td>
                                </tr>
                                <tr>
                                    <td>[[%foodbrain.forest.data_size]]</td>
                                    <td>[[+data.size:append=` hectare`]]</td>
                                </tr>
                                <tr>
                                    <td>[[%foodbrain.forest.data_age]]</td>
                                    <td>[[+data.date_start:ago=`%Y`:stripString=` ago`]]</td>
                                </tr>
                                <tr>
                                    <td>[[%foodbrain.forest.data_access]]</td>
                                    <td>[[+data.access:renderInputOption=`alias`]]</td>
                                </tr>
                                <tr>
                                    <td class="top aligned">[[%foodbrain.address.heading]]</td>
                                    <td>
                                        [[modifiedIf:append=`<br>`?
                                            &subject=`[[+data.Address_house_nr]][[+data.Address_street]][[+data.Address_subdivision]]`
                                            &operator=`notempty`
                                            &then=`[[+data.Address_house_nr]] [[+data.Address_street:append=`, `]][[+data.Address_subdivision]]`
                                        ]]
                                        [[+data.Address_locality]][[+data.Address_lgu:prepend=`, `]]
                                    </td>
                                </tr>
                                <tr>
                                    <td>[[%foodbrain.location.elevation]]</td>
                                    <td>[[+data.Location_elevation:append=` meters`]]</td>
                                </tr>
                            </table>

                            [[+data.website:notempty=`<a href="[[+data.website]]" class="ui basic button" target="_blank"><i class="globe icon"></i>[[%foodbrain.forest.data_website]]</a>`]]
                            [[+data.facebook:notempty=`<a href="[[+data.facebook]]" class="ui basic button" target="_blank"><i class="facebook f icon"></i>[[%foodbrain.forest.data_facebook]]</a>`]]
                        </div>

                        <div id="forest-location" class="column">

                            <div id="forest-map"></div>

                            <script>
                                window.addEventListener('DOMContentLoaded', function() {
                                    const map = new L.Map('forest-map', {
                                        scrollWheelZoom: false
                                    });

                                    // Load tile layer
                                    const tiles = new L.tileLayer('https://api.mapbox.com/styles/v1/{username}/{id}/tiles/{z}/{x}/{y}{r}?access_token={accessToken}', {
                                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                                        tileSize: 512,
                                        zoomOffset: -1,
                                        maxZoom: 18,
                                        id: '[[++foodbrain.mapbox_style_id]]',
                                        username: '[[++foodbrain.mapbox_username]]',
                                        accessToken: '[[++foodbrain.mapbox_access_token]]'
                                    });

                                    // Load markers with GeoJSON
                                    let geoJsonForest = [{
                                        "type": "Feature",
                                        "properties": {
                                            "name": "[[*pagetitle]]",
                                            "amenity": "",
                                            "id": "forest-[[*forest_global_id:empty=`[[*id]]`]]"
                                        },
                                        "geometry": {
                                            "type": "Point",
                                            "coordinates": [[[+data.Location_lng]], [[+data.Location_lat]]]
                                        }
                                    }];

                                    // Output the map
                                    map.addLayer(tiles);
                                    map.setView([[[+data.Location_lat]], [[+data.Location_lng]]], 11);

                                    // Add marker
                                    L.geoJSON(geoJsonForest).addTo(map);

                                    // Only activate mousewheel scrolling on focus
                                    map.on('focus', function () {
                                        map.scrollWheelZoom.enable();
                                    });
                                    map.on('blur', function () {
                                        map.scrollWheelZoom.disable();
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        [[*neighbors_visibility:eq=`1`:then=`
        <nav id="menu-neighbors" class="ui large fluid two item menu">
            [[pdoNeighbors?
                &loop=`1`
                &tplPrev=`neighborNavItemPrev`
                &tplNext=`neighborNavItemNext`
                &tplWrapper=`@INLINE [[+prev]][[+next]]`
                &sortby=`publishedon`
                &sortdir=`asc`
            ]]
        </nav>
        `]]
    </main>

    [[[[modifiedIf?
        &subject=`footerTheme`
        &operator=`iselement`
        &operand=`chunk`
        &then=`$footerTheme`
        &else=`$footer`
    ]]]]
</div>

[[loadAssets? &component=`map`]]
[[loadAssets? &component=`table`]]
[[$script]]

</body>
</html>