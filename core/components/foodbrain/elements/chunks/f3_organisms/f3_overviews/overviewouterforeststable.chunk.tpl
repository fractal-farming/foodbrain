[[setBoxType? &input=`[[+row_tpl]]` &prefix=`ft_[[+layout_id]]_[[+unique_idx]]`]]

[[Switch:toPlaceholder=`[[+prefix]].sortdir`?
    &get=`[[+sortby]]_[[+sortdir]]`
    &c1=`menuindex_0`     &do1=`ASC`
    &c2=`publishedon_0`   &do2=`DESC`
    &c3=`createdon_0`     &do3=`DESC`
    &c4=`pagetitle_0`     &do4=`ASC`

    &c5=`menuindex_1`     &do5=`DESC`
    &c6=`publishedon_1`   &do6=`ASC`
    &c7=`createdon_1`     &do7=`ASC`
    &c8=`pagetitle_1`     &do8=`DESC`

    &default=`DESC`
]]

<table id="[[If? &subject=`[[+prefix]]` &operator=`is` &operand=`ft__` &then=`ft_[[Time]]` &else=`[[+prefix]]`]]"
       class="ui [[+[[+prefix]].grid_settings]] [[+padding:replace=`relaxed==padded`]] overview [[+[[+prefix]].box_type]]"
    >
    [[$[[+row_tpl]]Head?
        &db_fields=`[[+[[+prefix]].dataset_content]]`
    ]]
    <tbody>
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`overviews``]]

        &parents=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`0` &else=`[[++foodbrain.forest_container_id]]`]]`
        &resources=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`[[+resources]]` &else=`null`]]`

        &depth=`99`
        &limit=`[[+limit:default=`0`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowForest[[+[[+prefix]].row_type]]`
        &where=`{"template:IN":[100001,100004]}`
        &includeTVs=`
            forest_global_id,
            overview_img_landscape,
            overview_img_portrait,
            overview_img_square,
            overview_img_wide,
            overview_img_pano,
            overview_link_text
        `
        &processTVs=`0`
        &tvFilters=`[[*context_key:isnot=`forestbrain`:then=`forest_global_id>>0`]]`
        &tvPrefix=``
        &showHidden=`[[+show_hidden:default=`1`]]`

        &leftJoin=`{
            "GlobalTV": {
                "class": "modTemplateVarResource",
                "on": "modResource.id = GlobalTV.contentid AND GlobalTV.tmplvarid = 100012"
            },
            "Data": {
                "class": "foodForest",
                "on": "GlobalTV.value = Data.resource_id"
            },
            "Location": {
                "class": "foodLocation",
                "on": "Data.location_id = Location.id"
            },
            "Address": {
                "class": "foodAddress",
                "on": "Data.address_id = Address.id"
            },
            "Contact": {
                "class": "modUserProfile",
                "on": "Data.contact_id = Contact.internalKey"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "modResource.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`modResource.id`
        &select=`{
            "Data": "Data.category AS category, Data.size AS size, Data.website AS website, Data.facebook AS facebook, Data.img_landscape AS image",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "Address": "Address.id AS address, Address.locality AS locality, Address.lgu AS lgu",
            "Contact": "Contact.fullname AS contact",
            "CreatedBy": "CreatedBy.fullname AS maintainer",
            "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
        }`
        &showLog=`0`

        &prepareSnippet=`overviewPrepareForests`
        &tplPlantRow=`foodForestPlantBasic`

        &sortby=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`FIELD(modResource.id, [[+resources]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        &row_tpl=`[[If? &subject=`[[$[[+row_tpl]]Theme]]` &operator=`isnull` &then=`[[+row_tpl]]` &else=`[[+row_tpl]]Theme`]]`
        &cols=`[[+cols:textToNumber]]`
        &box_type=`[[+[[+prefix]].box_type]]`
        &row_type=`[[+[[+prefix]].row_type]]`
        &column_type=`[[+[[+prefix]].column_type]]`
        &prefix=`[[+prefix]]`
        &unique_idx=`[[+unique_idx]]`
        &title_field=`[[+title_field]]`
        &title_hierarchy=`[[+title_hierarchy]]`
        &show_subtitle=`[[+show_subtitle]]`
        &show_introtext=`[[+show_introtext]]`
        &link_text=`[[If? &subject=`[[+link_text]]` &operator=`isnull` &then=`0` &else=`[[+link_text]]`]]`
        &img_type=`[[+img_type]]`
        &padding=`[[+padding]]`
        &de_emphasize=`[[+de_emphasize]]`
        &lazy_load=`[[+lazy_load]]`

        [[+pagination:eq=`1`:then=`
        &ajaxMode=`[[+pagination_type:default=`default`]]`
        &ajaxElemWrapper=`#[[+prefix]]`
        &ajaxElemRows=`#[[+prefix]] tbody`
        &ajaxElemPagination=`#[[+prefix]] tfoot.pagination-table`
        &ajaxElemLink=`#[[+prefix]] .pagination a`
        &ajaxElemMore=`#[[+prefix]] .more`
        &scrollTop=`0`

        &ajaxTplMore=`pageNavItemLoadMoreTable`
        &tplPageWrapper=`pageNavWrapperTable`
        &tplPage=`pageNavItem`
        &tplPageActive=`pageNavItemActive`
        &tplPageFirst=`pageNavItemIcon@PaginationFirst`
        &tplPageLast=`pageNavItemIcon@PaginationLast`
        &tplPagePrev=`pageNavItemIcon@PaginationPrev`
        &tplPageNext=`pageNavItemIcon@PaginationNext`
        &tplPageSkip=`pageNavItemDisabled`
        &tplPageFirstEmpty=`pageNavItemIconDisabled@PaginationFirst`
        &tplPageLastEmpty=`pageNavItemIconDisabled@PaginationLast`
        &tplPagePrevEmpty=`pageNavItemIconDisabled@PaginationPrev`
        &tplPageNextEmpty=`pageNavItemIconDisabled@PaginationNext`

        &pageVarKey=`[[+prefix]]-page`
        &pageNavVar=`[[+prefix]].page.nav`
        `]]
    ]]
    </tbody>
    <tfoot class="pagination-table">
        [[+pagination:eq=`1`:then=`[[!+[[+prefix]].page.nav]]`]]
    </tfoot>
</table>

[[loadAssets? &component=`table`]]
