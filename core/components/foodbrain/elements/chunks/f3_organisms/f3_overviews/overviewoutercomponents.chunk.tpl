[[setBoxType? &input=`[[+row_tpl]]` &prefix=`co_[[+layout_id]]_[[+unique_idx]]`]]

[[Switch:toPlaceholder=`[[+prefix]].sortdir`?
    &get=`[[+sortby]]_[[+sortdir]]`
    &c1=`menuindex_0`     &do1=`ASC`
    &c2=`publishedon_0`   &do2=`DESC`
    &c3=`createdon_0`     &do3=`DESC`
    &c4=`pagetitle_0`     &do4=`ASC`

    &c5=`menuindex_1`     &do5=`DESC`
    &c6=`publishedon_1`   &do6=`ASC`
    &c7=`createdon_1`     &do7=`ASC`
    &c8=`pagetitle_1`     &do8=`DESC`

    &default=`DESC`
]]

[[+pagination:eq=`1`:then=`
<div id="[[If? &subject=`[[+prefix]]` &operator=`is` &operand=`co__` &then=`co_[[Time]]` &else=`[[+prefix]]`]]" class="pagination-wrapper">
`]]
    <div class="ui [[+cols]] [[+[[+prefix]].grid_settings]] [[+responsive:replace=`,== `]] [[+padding]] nested overview [[+[[+prefix]].box_type]]">
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`overviews``]]

        &parents=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`0` &else=`[[+parent]]`]]`
        &resources=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`[[+resources]]` &else=`null`]]`

        &depth=`0`
        &limit=`[[+limit:default=`0`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowComponent[[+[[+prefix]].row_type]]`
        &where=`{"template":"[[++foodbrain.component_template_id]]"}`
        &includeTVs=`
            overview_img_landscape,
            overview_img_portrait,
            overview_img_square,
            overview_img_wide,
            overview_img_pano,
            overview_link_text
        `
        &processTVs=`0`
        &tvFilters=``
        &tvPrefix=``
        &showHidden=`[[+show_hidden:default=`1`]]`

        &leftJoin=`{
            "Data": {
                "class": "foodForestComponent",
                "on": "modResource.id = Data.resource_id"
            },
            "Forest": {
                "class": "foodForest",
                "on": "Data.forest_id = Forest.resource_id"
            },
            "Location": {
                "class": "foodLocation",
                "on": "Data.location_id = Location.id"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "modResource.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`modResource.id`
        &select=`{
            "Data": "Data.zone AS zone, Data.type AS type, Data.stage AS stage, Data.img_[[+img_type]] AS image",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "CreatedBy": "CreatedBy.fullname AS maintainer",
            "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
        }`
        &showLog=`0`

        &prepareSnippet=`overviewPrepareComponents`

        &sortby=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`FIELD(modResource.id, [[+resources]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </div>
[[[[+pagination:eq=`1`:then=`$paginationFluid:append=`
</div>
`? &prefix=`[[+prefix]]``]]]]
