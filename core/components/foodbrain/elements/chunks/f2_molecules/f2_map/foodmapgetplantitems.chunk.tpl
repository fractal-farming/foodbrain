[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`maps``]]
    &class=`foodForestPlantItem`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`foodMapPlantItemGeoJSON`
    &tplWrapper=`foodMapWrapperGeoJSON`
    &where=`[
        [[+forest_id:notempty=`
        {"Plant.forest_id:=":[[+forest_id]]},
        `]]
        {"Location.lat:!=":""},
        {"Location.lng:!=":""},
        {"published:=":1}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Plant": {
            "class": "foodForestPlant",
            "on": "foodForestPlantItem.plant_id = Plant.id"
        },
        "Taxon": {
            "class": "foodSpecies",
            "on": "Plant.taxonomy_id = Taxon.id"
        },
        "Location": {
            "class": "foodLocation",
            "on": "foodForestPlantItem.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "foodForestPlantItem.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`Plant.id`
    &select=`{
        "Taxon": "Taxon.name AS name, Taxon.name_local AS name_local",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
        "CreatedBy": "CreatedBy.fullname AS maintainer",
        "Plant": "Plant.id AS id"
    }`

    &showLog=`0`
]]