[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`maps``]]
    &class=`foodForestFeature`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`foodMapFeatureGeoJSON`
    &tplWrapper=`foodMapWrapperGeoJSON`
    &where=`[
        [[+forest_id:notempty=`
        {"forest_id:=":[[+forest_id]]},
        `]]
        {"published:=":1}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Location": {
            "class": "foodLocation",
            "on": "foodForestFeature.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "foodForestFeature.createdby = CreatedBy.internalKey"
        }
    }`
    &select=`{
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.geojson AS geojson",
        "CreatedBy": "CreatedBy.fullname AS maintainer",
        "foodForestFeature": "id,title,description,type,published,publishedon"
    }`

    &tplPopupContent=`foodMapFeaturePopupContent`
    &prepareSnippet=`foodMapPrepareFeatures`

    &showLog=`0`
]]