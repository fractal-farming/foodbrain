<p class="lead">
    [[+count]] upcoming event[[+count:gte=`2`:then=`s`]]:
</p>
<div id="events" class="ui one stackable horizontal cards center when stacked">
    [[+output]]
</div>
