[[!ChangePassword?
    &submitVar=`set-password`
    &fieldOldPassword=`password-old`
    &fieldNewPassword=`password-new`
    &fieldConfirmNewPassword=`password-confirm`
    &placeholderPrefix=`cp.`
    &validateOldPassword=`[[+validate_old:default=`0`]]`
    &validate=`username:blank`
    &reloadOnSuccess=`0`
    &postHooks=``
]]

[[-!+cp.error_message:notempty=`
<div class="ui error message">[[!+cp.error_message]]</div>
`]]
[[!+cp.successMessage:is=``:then=`
<div class="ui padded segment">
    <form id="set-password" class="ui [[+form_size:default=`large`]] form" action="[[~[[*id]]]]" method="post">
        <div class="sweetened field">
            <label for="username" class="sweetened">If you're human, keep this field blank!</label>
            <input type="text" name="username" id="username" value="" class="hidden" />
        </div>
        [[+validate_old:eq=`1`:then=`
        <div class="field [[!+cp.error.old:notempty=`error`]]">
            <label for="password-old">Old password</label>
            <input type="password" name="password-old" id="password-old" value="[[+cp.password-old]]" />
            <div class="help error">[[!+cp.error.password-old]]</div>
        </div>
        `]]
        <div class="field [[!+cp.error.password-new:notempty=`error`]]">
            <label for="password-new">[[+validate_old:eq=`1`:then=`New p`:else=`P`]]assword</label>
            <input type="password" name="password-new" id="password-new" value="[[+cp.password-new]]" />
            <div class="help error">[[!+cp.error.password-new]]</div>
        </div>
        <div class="field [[!+cp.error.password-confirm:notempty=`error`]]">
            <label for="password-confirm">Confirm password</label>
            <input type="password" name="password-confirm" id="password-confirm" value="[[+cp.password-confirm]]" />
            <div class="help error">[[!+cp.error.password-confirm]]</div>
        </div>
        <div class="field">
            <div class="ui error message"></div>
            <input class="ui large primary button" type="submit" name="set-password" value="Save" />
            <input class="ui large basic button" type="button" name="generate-password" id="generate-password" value="Generate password" />
        </div>
    </form>
</div>
`:else=`
<div class="ui positive message">
    <p><strong>Thanks!</strong></p>
    <p>Keep an eye on your email, to learn how we can help the trees together.</p>
    <a href="[[~125]]" class="ui big positive fluid button">Go to the dashboard</a>
</div>
`]]

<div class="ui mini password modal">
    <div class="header"><i class="lock icon"></i> Your password</div>
    <div class="content">
        <div class="ui large password header"></div>
        <p>This password will only be displayed once.<br>Please store it in a secure location right away.</p>
    </div>
    <div class="actions">
        <div class="ui basic cancel button">Cancel</div>
        <div class="ui positive approve button">Got it</div>
    </div>
</div>

[[fbLoadAssets]]
[[loadAssets? &component=`modal`]]
[[loadAssets? &component=`custom` &js=`packages/foodbrain/assets/components/foodbrain/js/password-generator.js`]]
[[loadAssets? &component=`custom` &js=`packages/foodbrain/assets/components/foodbrain/js/change-password.js`]]