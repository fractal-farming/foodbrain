<td data-title="[[%foodbrain.forest.heading]]">
    [[+img_landscape:notempty=`
    <figure class="ui image">
        <img class="ui mini image" src="[[ImagePlus? &value=`[[+imageJSON]]` &options=`w=200&zc=1` &type=`thumb`]]" alt="[[+pagetitle]]">
    </figure>
    `]]
    [[+pagetitle]]<br>
    <em class="meta">[[+locality:append=`, `]][[+lgu]]</em>
</td>

<td data-title="[[%foodbrain.forest.trees]]">
    [[+plants]]
</td>

<td data-title="[[%foodbrain.forest.contact]]">
    [[+contact]]
</td>

<td data-title="[[%foodbrain.forest.maintainer]]">
    [[+maintainer]]
</td>

[[-
<td>
    <a href="[[~[[+id]]]]" class="ui compact primary button">
        [[%foodbrain.map.button_read_more]]
    </a>
</td>
]]