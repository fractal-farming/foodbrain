[[$imgOverviewForestLink?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

<div class="center aligned content title">
    <a href="[[~[[+id]]]]" class="header">[[+pagetitle]]</a>
    [[If?
        &subject=`[[+show_subtitle]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<p class="meta">[[+locality]], [[+lgu]]</p>`
    ]]

    [[If?
        &subject=`[[+show_introtext]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<div class="description">[[+introtext]]</div>`
    ]]
</div>

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview? &classes=`large primary bottom attached``
]]]]