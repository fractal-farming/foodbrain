<figure class="ui rounded [[+img_type:replace=`_== `]] dimmable image">
    <div class="ui dimmer">
        <div class="content">
            <div class="center">
                [[[[If?
                    &subject=`[[+link_text]]`
                    &operator=`isnot`
                    &operand=`0`
                    &then=`$buttonHrefOverview:prepend=`<p>`:append=`</p>`? &classes=`small inverted``
                ]]]]
            </div>
        </div>
    </div>
    [[$imgOverviewForest? &uid=`[[+unique_idx]]`]]
</figure>
