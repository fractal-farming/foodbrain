[[$imgOverviewForestLink?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

[[[[If?
    &subject=`[[+show_subtitle]]`
    &operator=`EQ`
    &operand=`1`
    &then=`$headingHierarchySubtitleLink? &uid=`[[+unique_idx]]``
    &else=`$headingHierarchyLink? &uid=`[[+unique_idx]]``
]]]]

[[[[If?
    &subject=`[[+show_introtext]]`
    &operator=`EQ`
    &operand=`1`
    &then=`$introtextDescription? &uid=`[[+unique_idx]]``
]]]]

<div class="ui middle aligned list">
    <div class="item">[[%foodbrain.component.data_zone]]: [[+zone]]</div>
    <div class="item">[[%foodbrain.component.data_type]]: [[+type]]</div>
    <div class="item">[[%foodbrain.component.data_stage]]: [[+stage]]</div>
</div>

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview:prepend=`<p>`:append=`</p>`? &classes=`large primary``
]]]]