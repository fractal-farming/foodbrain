[[+Resource_pagetitle:notempty=`
[[+Resource_pagetitle]]
<i class="icon icon-fw icon-cube" title="Component ([[+id]])" style="float:right;"></i>
`]]
[[+Taxonomy_id:notempty=`
[[+Taxonomy_name:empty=`[[+Taxonomy_name_local]]`]] [[+Taxonomy_category]]
<i class="icon icon-fw icon-leaf" title="Plant ([[+id]])" style="float:right;"></i>
`]]
<i class="icon icon-fw icon-info-circle"
   title="[[+Output_availability:empty=`unknown`:ucfirst:append=` availability`]], [[+Output_quality:empty=`unknown`:append=` quality`]]"
   style="float:right;">
</i>