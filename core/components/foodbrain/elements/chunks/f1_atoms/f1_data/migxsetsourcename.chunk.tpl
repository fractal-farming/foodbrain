[[Switch?
    &get=`[[+type]]`
    &c1=`forest` &do1=`[[#[[+forest_id]].pagetitle:append=`<i class="icon icon-fw icon-tree" title="Forest ([[+forest_id]])" style="float:right;"></i> `]]`
    &c2=`person` &do2=`[[+username:append=`<i class="icon icon-fw icon-[[+published:eq=`1`:then=`user-o`:else=`user-secret`]]" title="Guardian" style="float:right;"></i> `]]`
    &c3=`store` &do3=`[[+name:append=`<i class="icon icon-fw icon-shopping-basket" title="Store" style="float:right;"></i> `]]`
    &c4=`market` &do4=`[[+name:append=`<i class="icon icon-fw icon-shopping-bag" title="Market" style="float:right;"></i> `]]`
    &c5=`event` &do5=`[[+name:append=`<i class="icon icon-fw icon-calendar" title="Event" style="float:right;"></i> `]]`
    &c6=`restaurant` &do6=`[[+name:append=`<i class="icon icon-fw icon-cutlery" title="Restaurant" style="float:right;"></i> `]]`
    &default=`[[%foodbrain.source.unknown]] <i class="icon icon-fw icon-map-marker" title="Unknown" style="float:right;"></i>`
]]