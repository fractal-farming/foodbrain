[[+quantity]]
[[+stage:in=`seed,seedling`:then=`[[+stage]][[+quantity:gt=`1`:then=`s`]]`:else=`[[+stage]]`]]
[[migxLoopCollection?
    &packageName=`foodbrain`
    &classname=`foodSource`
    &tpl=`@CODE:<i class="icon icon-fw icon-info-circle" title="Origin: [[+name]]" style="float:right;"></i>`
    &where=`{"id":"[[+source_id]]"}`
]]
<span style="float:right;">
    [[+published:isnot=`1`:then=`<i class="icon icon-fw icon-lock" title="Not visible to others"></i>`:else=`<i class="icon icon-fw"></i>`]]
    [[+zone:isnot=``:then=`<i class="icon icon-fw icon-bullseye" title="Zone: [[+zone]]"></i>`:else=`<i class="icon icon-fw"></i>`]]
</span>
