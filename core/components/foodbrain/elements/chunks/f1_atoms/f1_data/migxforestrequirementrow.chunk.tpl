[[+Requirement_title]]
<span style="float:right;clear:right;">
    [[+Requirement_published:isnot=`1`:then=`
    <i class="icon icon-fw icon-lock" title="Not visible to others"></i>
    `:else=`
    <i class="icon icon-fw"></i>
    `]]
    [[+Requirement_priority:eq=`3`:then=`
    <i class="icon icon-fw icon-heartbeat" title="Need"></i>
    `:else=`
    <i class="icon icon-fw icon-heart-o" title="Want"></i>
    `]]
</span>