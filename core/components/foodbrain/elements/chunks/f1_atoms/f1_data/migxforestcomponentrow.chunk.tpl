<a href="?a=resource/update&id=[[+Component_resource_id]]"><i class="icon icon-fw icon-edit" title="Edit component"></i></a>
[[#[[+Component_resource_id]].pagetitle]]
<span style="float:right;clear:right;">
    [[#[[+Component_resource_id]].published:isnot=`1`:then=`<i class="icon icon-fw icon-lock" title="Not visible to others"></i>`:else=`<i class="icon icon-fw"></i>`]]
    [[+Component_zone:isnot=``:then=`<i class="icon icon-fw icon-bullseye" title="Zone: [[+Component_zone]]"></i>`:else=`<i class="icon icon-fw"></i>`]]
</span>