[[$migxSetSourceName?
    &type=`[[+Source_type]]`
    &name=`[[+Source_name]]`
    &forest_id=`[[+Source_forest_id]]`
    &username=`[[+Source_contact_id:userinfo=`fullname`]]`
    &published=`[[+Source_published]]`
    &idx=`[[+id]]`
]]
<i class="icon icon-fw icon-info-circle"
   title="[[+availability:empty=`unknown`:ucfirst:append=` availability`]], [[+quality:empty=`unknown`:append=` quality`]]"
   style="float:right;">
</i>