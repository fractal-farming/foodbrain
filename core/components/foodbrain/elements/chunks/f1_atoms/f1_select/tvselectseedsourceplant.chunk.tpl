--- Select parent plant ---==||
[[pdoResources?
    &class=`foodForestPlantItem`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`tvSelectSeedSourcePlantRow`
    &where=`[
        {"createdby:=":"[[+modx.user.id]]"}
    ]`
    &outputSeparator=`||`
    &sortby=`id`

    &leftJoin=`{
        "Plant": {
            "class": "foodForestPlant",
            "on": "foodForestPlantItem.plant_id = Plant.id"
        },
        "Taxon": {
            "class": "foodSpecies",
            "on": "Plant.taxonomy_id = Taxon.id"
        },
        "Location": {
            "class": "foodLocation",
            "on": "foodForestPlantItem.location_id = Location.id"
        },
        "Resource": {
            "class": "modResource",
            "on": "Plant.forest_id = Resource.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "foodForestPlantItem.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`Plant.id`
    &select=`{
        "Plant": "Plant.id AS plant_id",
        "Taxon": "Taxon.name AS name, Taxon.name_local AS name_local",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
        "Forest": "Resource.id AS forest_id, Resource.pagetitle AS forest_name",
        "CreatedBy": "CreatedBy.fullname as owner",
        "foodForestPlantItem": "id,published"
    }`

    &showLog=`0`
]]