{
    "type": "Feature",
    "properties": {
        "name": "[[+title]]",
        "amenity": "",
        "popupContent": [[+popup_content]],
        "id": "feature-[[+id]]"
    },
    [[+geometry]]
}