{
    "type": "Feature",
    "properties": {
        "name": "[[+pagetitle]]",
        "amenity": "",
        "popupContent": [[+popup_content]],
        "id": "forest-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}