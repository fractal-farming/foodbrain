{
    "type": "Feature",
    "properties": {
        "name": "[[+title]]",
        "amenity": "",
        "popupContent": "[[$foodMapEventPopupContent:strip:replace=`/==\/`? &idx=`[[+idx]]`]]",
        "id": "event-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+location]] ]
    }
}