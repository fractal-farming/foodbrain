{
    "type": "Feature",
    "properties": {
        "name": "[[+name]]",
        "amenity": "",
        "popupContent": "",
        "id": "plant-item-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}