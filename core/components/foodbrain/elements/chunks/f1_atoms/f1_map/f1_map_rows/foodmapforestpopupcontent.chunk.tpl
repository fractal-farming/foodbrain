[[+image]]
<p>
    <strong>[[+pagetitle]]</strong><br>
    <span class="meta"><i class="tag icon"></i>[[+category]]</span>
    <span class="meta"><i class="marker icon"></i>[[+locality]][[+lgu]]</span>
</p>
[[+message]]
<div class="ui middle aligned equal width grid">
    <div class="compact column">
        <a href="[[+link]]" class="ui compact mini primary button">
            [[%foodbrain.map.button_read_more]]
        </a>
    </div>
    <div class="right aligned column">
        <div class="ui compact basic mini icon buttons">
            [[+button_website]]
            [[+button_facebook]]
        </div>
    </div>
</div>