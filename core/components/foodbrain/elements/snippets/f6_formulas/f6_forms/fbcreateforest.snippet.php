<?php
$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;



$foodbrain->resetNull($object, $properties);
$foodbrain->saveLocation($object, $properties, $locationID);

return json_encode($result);