<?php
/**
 * @var string $input
 */
switch($input) {
    case stripos($input,'ForestBasicCard') !== false:
    case stripos($input,'ComponentBasicCard') !== false:
        $box_type = "cards";
        $row_type = "";
        $column_type = "card";
        $grid_settings = "";
        break;
    case stripos($input,'ForestWideCard') !== false:
        $box_type = "horizontal cards";
        $row_type = "";
        $column_type = "card";
        $grid_settings = "[[+de_emphasize:ne=`1`:then=`raised`]]";
        break;
    case stripos($input,'ForestWideSegment') !== false:
        $box_type = "basic segments";
        $row_type = "";
        $column_type = "ui [[+padding:replace=`relaxed==padded`]] segment";
        $grid_settings = "[[+de_emphasize:ne=`1`:then=`raised`]]";
        break;
    case stripos($input,'ForestTable') !== false:
    case stripos($input,'ComponentTable') !== false:
        $box_type = "sortable table";
        $row_type = "table";
        $column_type = "";
        $grid_settings = "small very compact [[+de_emphasize:eq=`1`:then=`very basic`]]";
        break;
    case stripos($input,'ForestBasic') !== false:
    case stripos($input,'ComponentBasic') !== false:
        $box_type = "grid";
        $row_type = "";
        $column_type = "column";
        $grid_settings = "column";
        break;
    default:
        return '';
}

return [
    'box_type' => $box_type,
    'row_type' => $row_type,
    'column_type' => $column_type,
    'grid_settings' => $grid_settings,
];