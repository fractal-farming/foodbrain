<?php
$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->addPackage('foodbrain',$corePath . 'model/');

$input = $modx->getOption('input', $scriptProperties, '');
$resource = $modx->getObject('modResource', array('uri' => $input));
$resourceID = $resource->get('id');
$forest = $modx->getObject('foodForest', array('resource_id' => $resourceID));

if ($forest) {
    // Get image from forest object
    $image = $forest->get('img_landscape');

    // Decode image and fix media source path
    $image = json_decode($image, true);
    $imagePath = 'uploads/img/forest/' . $resourceID . '/overview/' . $image['sourceImg']['src'];

    // Thumb images before output
    if ($image['sourceImg']['src']) {
        return $modx->runSnippet('pThumb', array(
            'input' => $imagePath,
            'options' => 'w=600,zc=1',
        ));
    }
}

return '';