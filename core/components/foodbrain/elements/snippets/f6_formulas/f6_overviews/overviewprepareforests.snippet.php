<?php
/**
 * overviewPrepareForests
 *
 * Modify field values before the pdoResources snippet in overviewOuterForests
 * is executed.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$fbCorePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$rmCorePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$foodbrain = $modx->getService('foodbrain','FoodBrain',$fbCorePath . 'model/foodbrain/',array('core_path' => $fbCorePath));
$romanesco = $modx->getService('romanesco','Romanesco',$rmCorePath . 'model/romanescobackyard/',array('core_path' => $rmCorePath));

if (!($foodbrain instanceof FoodBrain)) return;

$tplPlantRow = $modx->getOption('tplPlantRow', $scriptProperties, 'foodForestPlantBasic');

//$modx->log(modX::LOG_LEVEL_ERROR, '[overviewPrepareForests] Row: ' . print_r($row, 1));
//$row = array();

// Links
// =============================================================================

// Create buttons
$row['button_website'] = '';
$row['button_facebook'] = '';
if ($row['website']) {
    $row['button_website'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['website'],
        'icon_class' => 'globe icon',
    ));
}
if ($row['facebook']) {
    $row['button_facebook'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['facebook'],
        'icon_class' => 'facebook f icon',
    ));
}

// Generate links
$row['link'] = $modx->makeUrl($row['id']);


// Images
// =============================================================================

if ($row['image']) {
    // Generate correct image path
    $image = $foodbrain->generateImagePath($row);

    $row['imageJSON'] = json_encode($image);
}
else {
    // Provide fallback
    $row['imageFallback'] = '/uploads/img/fallback/fallback_' . $row['img_type'] . '.png';
}


// Forest data
// =============================================================================

// Get plant species
$row['plants'] = $foodbrain->getPlants($row,$tplPlantRow,'<br>');

// Get category name
if ($row['category']) {
    $rmOption = $modx->getObject('rmOption', array(
        'key' => 'forest_category',
        'alias' => $row['category'],
    ));
    if ($rmOption) {
        $row['category'] = $rmOption->get('name');
    }
}


return json_encode($row);