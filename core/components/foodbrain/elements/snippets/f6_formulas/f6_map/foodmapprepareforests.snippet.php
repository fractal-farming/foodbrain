<?php
/**
 * foodMapPrepareForests
 *
 * Modify field values before the pdoResources snippet in foodMapGetForests is
 * executed.
 *
 * Please note that the output of foodMapPrepareForests is a GeoJSON object, so
 * the output of each field needs to be valid JSON.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$fbCorePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$rmCorePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$foodbrain = $modx->getService('foodbrain','FoodBrain',$fbCorePath . 'model/foodbrain/',array('core_path' => $fbCorePath));
$romanesco = $modx->getService('romanesco','Romanesco',$rmCorePath . 'model/romanescobackyard/',array('core_path' => $rmCorePath));

if (!($foodbrain instanceof FoodBrain)) return;

$tplPopupContent = $modx->getOption('tplPopupContent', $scriptProperties, '');

//$modx->log(modX::LOG_LEVEL_ERROR, '[foodMapPrepareForests] Row: ' . print_r($row, 1));
//$row = array();

// Coordinates
// =============================================================================

// Randomly generate nearby coordinates to obfuscate real location
if ($row['lat'] && $row['lng'] && $row['radius'] > 0) {
    $randomLocation = array();
    $randomLocation = $foodbrain->obfuscateCoordinates(array($row['lat'],$row['lng']), $row['radius']);

    $row['lat'] = $randomLocation['lat'];
    $row['lng'] = $randomLocation['lng'];

    $row['message'] = $modx->getChunk('richTextMessage', array(
        'size' => 'small',
        'message_type' => 'info',
        'heading' => $modx->lexicon('foodbrain.map.location_obfuscated_heading'),
        'content' => $modx->lexicon('foodbrain.map.location_obfuscated_content', array('radius'=>$row['radius'])),
    ));
}


// Popup content
// =============================================================================

// Create buttons
$row['button_website'] = '';
$row['button_facebook'] = '';
if ($row['website']) {
    $row['button_website'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['website'],
        'icon_class' => 'globe icon',
    ));
}
if ($row['facebook']) {
    $row['button_facebook'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['facebook'],
        'icon_class' => 'facebook f icon',
    ));
}

if ($row['image']) {
    // Generate correct image path
    $image = $foodbrain->generateImagePath($row);

    // Create thumbnail with ImagePlus
    $image = $modx->runSnippet('ImagePlus', array(
        'value' => json_encode($image),
        'options' => 'w=600&q=85&zc=1',
    ));

    // Create output
    $row['image'] = $modx->getChunk('foodMapPopupImage', array(
        'image' => $image,
        'alt' => $row['pagetitle'],
    ));
}

// Get plant species
$plantItems = array();
$plants = $modx->getCollectionGraph('foodForestPlant', '{"Taxonomy"}', array(
    'forest_id' => $row['id'],
));
foreach ($plants as $plant) {
    if ($plant->get('taxonomy_id')) {
        $plantItems[] = $plant->getOne('Taxonomy')->get('name');
    }
}
$row['plants'] = implode(', ',$plantItems);

// Get category name
if ($row['category']) {
    $rmOption = $modx->getObject('rmOption', array(
        'key' => 'forest_category',
        'alias' => $row['category'],
    ));
    if ($rmOption) {
        $row['category'] = $rmOption->get('name');
    }
}

// Generate links
$row['link'] = $modx->makeUrl($row['id']);

// Format LGU
$row['lgu'] = ', ' . $row['lgu'];

// Use chunk tpl for output
$row['popup_content'] = json_encode(
    $modx->getChunk($tplPopupContent, array(
        'image' => $row['image'],
        'message' => $row['message'],
        'introtext' => nl2br($row['introtext']),
        'plants' => nl2br($row['plants']),
        'button_website' => $row['button_website'],
        'button_facebook' => $row['button_facebook'],
    ))
);

return json_encode($row);