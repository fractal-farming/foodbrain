<?php
/**
 * migxSaveImportSource
 *
 * Aftersave snippet to connect source to an import.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = $modx->getOption('object', $scriptProperties, null);
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, array());

$co_id = $modx->getOption('co_id', $properties, NULL);

if (is_object($object)) {
    // Set key and ID of parent object
    $object->set('import_id', $co_id);
    $object->save();
}

$foodbrain->resetNull($object, $properties);

return '';