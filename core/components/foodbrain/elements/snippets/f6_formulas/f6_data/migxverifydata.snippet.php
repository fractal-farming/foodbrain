<?php
/**
 * migxVerifyData
 *
 * Hook that checks and corrects data before saving.
 */

$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = &$modx->getOption('object', $scriptProperties, null);
$properties = $modx->getOption('scriptProperties', $scriptProperties, '');
$configs = $modx->getOption('configs', $properties, '');

//$modx->log(modX::LOG_LEVEL_ERROR, 'Props: ' . print_r($properties, true));
//$modx->log(modX::LOG_LEVEL_ERROR, 'Configs: ' . $configs);
//$modx->log(modX::LOG_LEVEL_ERROR, 'Field: ' . print_r($field, true));

$data = json_decode($properties['data'], true);
//$modx->log(modX::LOG_LEVEL_ERROR, 'Data: ' . print_r($data, true));

// Format decimal keys
$decimalKeys = array(
    'size',
    'quantity',
    'price',
    'lat',
    'lng',
    'elevation',
);

foreach ($data as $key => $value) {
    // Change separator from , to . for decimals
    if (in_array($key,$decimalKeys) && stripos($value,',') == true) {
        $value = str_replace(',','.',$value);
        $modx->log(modX::LOG_LEVEL_ERROR, 'Changed decimal separator for: ' . $key);
        $object->set($key, $value);
    }

    // Reset empty decimals to NULL
    if (in_array($key,$decimalKeys) && $value === '') {
        $modx->log(modX::LOG_LEVEL_WARN, 'NULL was reset for: ' . $key);
        $object->set($key, NULL);
    }
}

// Save changes
$object->save();

// Validate JSON data
if (array_key_exists('Location_geojson', $properties)) {
    if ($properties['Location_geojson']) {
        $validateOutput = $foodbrain->validateJSON($properties['Location_geojson']);

        if ($validateOutput) {
            return json_encode($validateOutput);
        }
    }
}

return true;