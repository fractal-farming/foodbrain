<?php
/**
 * migxSaveInput
 *
 * Aftersave snippet for inputs. Inputs can be tied to a component or a plant.
 *
 * The MIGX config contains a key with the win_id of the parent object,
 * by which the parent class can be determined. The id forwarded by co_id
 * doesn't provide that information.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = $modx->getOption('object', $scriptProperties, null);
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());

//$modx->log(modX::LOG_LEVEL_ERROR, print_r($_REQUEST,1));
//$modx->log(modX::LOG_LEVEL_ERROR, print_r($properties,1));

switch ($properties['parent']) {
    case 'component':
        $object->set('component_id', $properties['co_id']);
        break;
    case 'plant':
        $object->set('plant_id', $properties['co_id']);
        break;
}

$object->save();

$foodbrain->resetNull($object, $properties);

return '';