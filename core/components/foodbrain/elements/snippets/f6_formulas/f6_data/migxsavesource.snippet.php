<?php
$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = &$modx->getOption('object', $scriptProperties, null);
$configs = $modx->getOption('configs', $properties, '');
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());
$postValues = $modx->getOption('postvalues', $scriptProperties, array());

$result = array();
$locationID = '';
$addressID = NULL;
$forestID = NULL;
$contactID = NULL;
$sourceType = '';

// Get IDs
if (is_object($object)) {
    $locationID = $object->get('location_id');
    $addressID = $object->get('address_id');
    $forestID = $object->get('forest_id');
    $contactID = $object->get('contact_id');
    $sourceType = $object->get('type');
}

$foodbrain->resetNull($object, $properties);
$foodbrain->saveLocation($object, $properties, $locationID);
$foodbrain->saveAddress($object, $properties, $addressID);

// Contact data can only be edited if the source type is set to 'person'
if ($sourceType == 'person') {
    $foodbrain->saveContact($object, $properties, $contactID);
}

return json_encode($result);