<?php
$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = $modx->getOption('object', $scriptProperties, null);
$configs = $modx->getOption('configs', $properties, '');
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());
$postValues = $modx->getOption('postvalues', $scriptProperties, array());

$result = array();
$userID = '';
$locationID = NULL;
$addressID = NULL;

// Get IDs
if (is_object($object)) {
    $userID = $object->get('user_id');
    $locationID = $object->get('location_id');
    $addressID = $object->get('address_id');
}

$foodbrain->resetNull($object, $properties);
$foodbrain->saveContact($object, $properties, $userID);
$foodbrain->saveAddress($object, $properties, $addressID);
$foodbrain->saveLocation($object, $properties, $locationID);

return json_encode($result);