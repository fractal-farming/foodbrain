<?php
/**
 * migxSaveComponent
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/', array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = $modx->getOption('object', $scriptProperties, null);
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, array());

if (!is_object($object)) return;

$result = array();
$resource = array();
$locationID = $object->get('location_id');

$resourceProperties = array(
    'pagetitle' => $postValues['Resource_pagetitle'],
    'longtitle' => '',
    'introtext' => $postValues['Resource_introtext'],
    'alias' => '',
    'content' => '',
    'richtext' => 1,
    'published' => $postValues['Resource_published'],
    'parent' => $properties['resource_id'],
    'template' => $modx->getOption('foodbrain.component_template_id', $scriptProperties),
    'searchable' => 1,
    'hidemenu' => 1,
    'show_in_tree' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'forestbrain',
);

// Create component resource if MIGX object is new
if ($properties['object_id'] == 'new') {
    $response = $modx->runProcessor('resource/create', $resourceProperties);

    if (!$response->isError()) {
        // Resource object only returns array with ID
        $resource = $response->getObject();

        if ($resource['id']) {
            // Add virtual sub folder to URI
            $resourceObject = $modx->getObject('modResource', $resource['id']);
            $resourceAlias = $resourceObject->get('alias');
            $resourceURI = $resourceObject->get('uri');
            $resourceURI = str_replace($resourceAlias, 'components/' . $resourceAlias, $resourceURI);

            $resourceObject->set('uri', $resourceURI);
            $resourceObject->set('uri_override', 1);
            $resourceObject->save();

            // Reference component resource in object
            $object->set('resource_id', $resource['id']);
        }

        $modx->log(MODX::LOG_LEVEL_INFO, 'Successfully created new resource: ' . $resourceProperties['pagetitle'], __METHOD__, __LINE__);
    } else {
        $modx->log(MODX::LOG_LEVEL_ERROR, 'Failed to create new resource: ' . $resourceProperties['pagetitle'] . ". Errors:\n" . implode("\n", $response->getAllErrors()), __METHOD__, __LINE__);
    }
}

// Update component resource if ID can be determined (not the case in quick edit!)
if ($properties['Resource_id'] && $object->get('Resource_id')) {
    $resourceProperties['id'] = $object->get('Resource_id');
    $response = $modx->runProcessor('resource/update', $resourceProperties);

    if (!$response->isError()) {
        $modx->log(MODX::LOG_LEVEL_INFO, 'Successfully updated resource: ' . $resourceProperties['pagetitle'], __METHOD__, __LINE__);
    } else {
        $modx->log(MODX::LOG_LEVEL_ERROR, 'Failed to update resource: ' . $resourceProperties['pagetitle'] . ". Errors:\n" . implode("\n", $response->getAllErrors()), __METHOD__, __LINE__);
    }
}

// Set forest ID
if ($properties['resource_id'] && !$object->get('forest_id')) {
    $object->set('forest_id', $properties['resource_id']);
}

$object->save();

$foodbrain->resetNull($object, $properties);
$foodbrain->saveLocation($object, $properties, $locationID);

return json_encode($result);