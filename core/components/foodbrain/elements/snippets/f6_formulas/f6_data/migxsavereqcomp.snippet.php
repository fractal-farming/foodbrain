<?php
/**
 * migxSaveReqComp
 *
 * Aftersave snippet for connecting requirements to components (many to many).
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = $modx->getOption('object', $scriptProperties, null);
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());

// Set component ID of object
$componentID = $properties['co_id'];

if (is_object($object) && $componentID) {
    $object->set('component_id', $componentID);
    $object->save();
}

return '';