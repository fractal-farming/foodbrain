<?php
$corePath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/');
$foodbrain = $modx->getService('foodbrain','foodbrain',$corePath . 'model/foodbrain/',array('core_path' => $corePath));

if (!($foodbrain instanceof FoodBrain)) return;

$object = &$modx->getOption('object', $scriptProperties, null);
$configs = $modx->getOption('configs', $properties, '');
$properties = $modx->getOption('scriptProperties', $scriptProperties, array());
$postValues = $modx->getOption('postvalues', $scriptProperties, array());

$result = array();
$locationID = NULL;
$addressID = NULL;
$contactID = NULL;

if (is_object($object)) {
    $locationID = $object->get('location_id');
    $addressID = $object->get('address_id');
    $contactID = $object->get('contact_id');

    $foodbrain->resetNull($object, $properties);

    $result = array_merge(
        $foodbrain->saveLocation($object, $properties, $locationID),
        $foodbrain->saveAddress($object, $properties, $addressID),
        $foodbrain->saveContact($object, $properties, $contactID)
    );

    // Update createdby values of related data when changing forest maintainer
    if ($properties['createdby'] != $properties['Resource_createdby']) {
        //$object->set('Resource_createdby', $properties['createdby']);
        //$object->set('Address_createdby', $properties['createdby']);
        //$object->set('Location_createdby', $properties['createdby']);

        // NB: setting a value on a joint field (like above) doesn't seem to work.
        $resource = $object->getOne('Resource');
        $resource->set('createdby', $properties['createdby']);
        $resource->save();

        $address = $object->getOne('Address');
        $address->set('createdby', $properties['createdby']);
        $address->save();

        $location = $object->getOne('Location');
        $location->set('createdby', $properties['createdby']);
        $location->save();

        // Also transfer ownership of connected plants
        $plants = $object->getMany('Plants');
        foreach ($plants as $plant) {
            $plant->set('createdby', $properties['createdby']);
            $plant->save();

            // Individual items
            $plantItems = $plant->getMany('Plants');
            foreach ($plantItems as $item) {
                $plant->set('createdby', $properties['createdby']);
                $plant->save();
            }
        }
    }
}

return json_encode($result);