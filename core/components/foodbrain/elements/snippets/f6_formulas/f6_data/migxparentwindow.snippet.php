<?php
/**
 * migxParentWindow
 *
 * Return the win_id value, which is the last bit of the parent window ID.
 * Make sure you don't use hyphenated IDs!
 */
$input = $modx->getOption('parent_window', $_REQUEST, 'undefined');
$output = explode('-', $input);
return array_pop($output);