<?php
/**
 * @package FoodBrain
 */
$xpdo_meta_map['foodUser']= array (
  'package' => 'foodbrain',
  'version' => '1.1',
  'extends' => 'modUser',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'composites' => 
  array (
    'ContactData' => 
    array (
      'class' => 'foodUserData',
      'local' => 'id',
      'foreign' => 'user_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
  ),
);
