<?php
/**
 * @package FoodBrain
 */
$xpdo_meta_map['foodForestReqComp']= array (
  'package' => 'foodbrain',
  'version' => '1.1',
  'table' => 'foodbrain_forest_req_comp',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'requirement_id' => 0,
    'component_id' => 0,
  ),
  'fieldMeta' => 
  array (
    'requirement_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'component_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'requirement_id' => 
    array (
      'alias' => 'requirement_id',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'requirement_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'component_id' => 
    array (
      'alias' => 'component_id',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'component_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Requirement' => 
    array (
      'class' => 'foodForestRequirement',
      'local' => 'requirement_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Component' => 
    array (
      'class' => 'foodForestComponent',
      'local' => 'component_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
