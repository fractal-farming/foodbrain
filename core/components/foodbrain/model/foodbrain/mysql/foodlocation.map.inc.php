<?php
/**
 * @package FoodBrain
 */
$xpdo_meta_map['foodLocation']= array (
  'package' => 'foodbrain',
  'version' => '1.1',
  'table' => 'foodbrain_locations',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'lat' => NULL,
    'lng' => NULL,
    'elevation' => NULL,
    'radius' => 0,
    'geojson' => NULL,
    'createdon' => 0,
    'createdby' => 0,
    'editedon' => 0,
    'editedby' => 0,
    'deleted' => 0,
  ),
  'fieldMeta' => 
  array (
    'lat' => 
    array (
      'dbtype' => 'decimal',
      'precision' => '8,6',
      'phptype' => 'float',
      'null' => true,
    ),
    'lng' => 
    array (
      'dbtype' => 'decimal',
      'precision' => '9,6',
      'phptype' => 'float',
      'null' => true,
    ),
    'elevation' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => true,
    ),
    'radius' => 
    array (
      'dbtype' => 'int',
      'precision' => '2',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'geojson' => 
    array (
      'dbtype' => 'mediumtext',
      'phptype' => 'json',
      'null' => true,
    ),
    'createdon' => 
    array (
      'dbtype' => 'int',
      'precision' => '20',
      'phptype' => 'timestamp',
      'null' => false,
      'default' => 0,
    ),
    'createdby' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'editedon' => 
    array (
      'dbtype' => 'int',
      'precision' => '20',
      'phptype' => 'timestamp',
      'null' => false,
      'default' => 0,
    ),
    'editedby' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'deleted' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'attributes' => 'unsigned',
      'phptype' => 'boolean',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'lat' => 
    array (
      'alias' => 'lat',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'lat' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => true,
        ),
      ),
    ),
    'lng' => 
    array (
      'alias' => 'lng',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'lng' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => true,
        ),
      ),
    ),
    'createdby' => 
    array (
      'alias' => 'createdby',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'createdby' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Forest' => 
    array (
      'class' => 'foodForest',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'Plants' => 
    array (
      'class' => 'foodForestPlantItem',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Features' => 
    array (
      'class' => 'foodForestFeature',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Zones' => 
    array (
      'class' => 'foodForestZone',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Components' => 
    array (
      'class' => 'foodForestComponent',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Sources' => 
    array (
      'class' => 'foodSource',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Contacts' => 
    array (
      'class' => 'foodUser',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'CreatedBy' => 
    array (
      'class' => 'modUser',
      'local' => 'createdby',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'EditedBy' => 
    array (
      'class' => 'modUser',
      'local' => 'editedby',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
