<?php

$xpdo_meta_map = array (
  'xPDOSimpleObject' => 
  array (
    0 => 'foodSpecies',
    1 => 'foodForest',
    2 => 'foodForestPlant',
    3 => 'foodForestPlantItem',
    4 => 'foodForestFeature',
    5 => 'foodForestRequirement',
    6 => 'foodForestZone',
    7 => 'foodForestComponent',
    8 => 'foodForestReqComp',
    9 => 'foodForestInput',
    10 => 'foodForestOutput',
    11 => 'foodForestImport',
    12 => 'foodForestImportSource',
    13 => 'foodSeed',
    14 => 'foodSource',
    15 => 'foodLocation',
    16 => 'foodAddress',
    17 => 'foodUserData',
  ),
  'modUser' => 
  array (
    0 => 'foodUser',
  ),
);