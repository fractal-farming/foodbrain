<?php
/**
 * @package FoodBrain
 */

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

use Seld\JsonLint\JsonParser;

class FoodBrain
{
    /**
     * @var modX|null $modx
     */
    public $modx = null;
    public $config = array();

    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;
        $corePath = $this->modx->getOption('foodbrain.core_path', $config, $this->modx->getOption('core_path') . 'components/foodbrain/');
        $this->config = array_merge(array(
            'basePath' => $this->modx->getOption('base_path'),
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
        ), $config);
        $this->modx->addPackage('foodbrain', $this->config['modelPath']);
    }

    // Prevent NULL values from being set to 0 in MIGXdb grids
    public function resetNULL($object, $properties, $prefix = '') {
        foreach ($properties as $key => $value) {
            $key = str_replace($prefix,'',$key);
            $objectValue = $object->get($key);

            // Reset to NULL if property value is empty and object value is 0
            if ($objectValue === 0 && $value === '') {
                //$this->modx->log(modX::LOG_LEVEL_ERROR, 'NULL was reset for: ' . $key);
                $object->set($key, NULL);
            }

            // Reset empty lat/long fields
            if (in_array($key,['lat','lng']) && $value === '') {
                //$this->modx->log(modX::LOG_LEVEL_ERROR, 'NULL was reset for: ' . $key);
                $object->set($key, NULL);
            }
        }
        $object->save();
    }

    /**
     * Generate correct image path for forest overviews
     *
     * @param array $properties
     * @return mixed
     */
    public function generateImagePath(array $properties) {
        // Use global ID if present
        $forestID = $properties['forest_global_id'] ?? $properties['id'];

        // Don't rely on media source to generate image path
        $properties['image']['sourceImg']['src'] = 'uploads/img/forest/' . $forestID . '/overview/' . $properties['image']['sourceImg']['src'];
        $properties['image']['sourceImg']['source'] = 1;

        return $properties['image'];
    }

    /**
     * Find all plants attached to given forest
     *
     * @param array $properties
     * @param string $rowTpl
     * @param string $divider
     * @return string
     */
    public function getPlants(array $properties, string $rowTpl = '', string $divider = ', ') {
        // Use global ID if present
        $forestID = $properties['forest_global_id'] ?? $properties['id'];

        $result = array();
        $plants = $this->modx->getCollectionGraph('foodForestPlant', '{"Taxonomy"}', array(
            'forest_id' => $forestID,
        ));

        foreach ($plants as $plant) {
            //if ($plant->get('taxonomy_id') && $plant->get('published') == 1) {
            if ($plant->get('taxonomy_id')) {
                $taxonomy = $plant->getOne('Taxonomy');

                if ($rowTpl) {
                    $result[] = $this->modx->getChunk($rowTpl, array(
                        'Taxonomy_name' => $taxonomy->get('name'),
                        'Taxonomy_name_local' => $taxonomy->get('name_local'),
                        'Taxonomy_name_latin' => $taxonomy->get('name_latin'),
                        'variety' => $plant->get('variety'),
                    ));
                } else {
                    $result[] = $taxonomy->get('name');
                }
            }
        }

        return implode($divider, $result);
    }

    /**
     * Find all inputs attached to given component
     *
     * @param array $properties
     * @param string $rowTpl
     * @param string $divider
     * @return string
     */
    public function getInputs(array $properties, string $rowTpl = '', string $divider = ', ') {
        $result = array();
        $inputs = $this->modx->getCollectionGraph('foodForestInput', '{"Output","Import"}', array(
            'component_id' => $properties['component_id'],
        ));

        foreach ($inputs as $input) {
            if ($input->get('output_id')) {
                $output = $input->getOne('Output');

                if ($rowTpl) {
                    $result[] = $this->modx->getChunk($rowTpl, array(
                        'Output_name' => $output->get('name'),
                        'Output_type' => $output->get('type'),
                        'Output_resilience' => $output->get('resilience'),
                        'Output_quality' => $output->get('quality'),
                        'Output_availability' => $output->get('availability'),
                        'type' => $input->get('type'),
                    ));
                } else {
                    $result[] = $output->get('name');
                }
            }
            if ($input->get('import_id')) {
                $import = $input->getOne('Import');

                if ($rowTpl) {
                    $result[] = $this->modx->getChunk($rowTpl, array(
                        'Import_name' => $import->get('name'),
                        'Import_resilience' => $import->get('resilience'),
                        'type' => $import->get('type'),
                    ));
                } else {
                    $result[] = $import->get('name');
                }
            }
        }

        return implode($divider, $result);
    }

    /**
     * Find all outputs attached to given component
     *
     * @param array $properties
     * @param string $rowTpl
     * @param string $divider
     * @return string
     */
    public function getOutputs(array $properties, string $rowTpl = '', string $divider = ', ') {
        $result = array();
        $outputs = $this->modx->getCollection('foodForestOutput', array(
            'component_id' => $properties['component_id'],
        ));

        foreach ($outputs as $output) {
            $result[] = $output->get('name');
        }

        return implode($divider, $result);
    }

    // Create and edit locations inside referencing MIGXdb configs
    public function saveLocation($object, $properties, $locationID) {
        if (is_object($object)) {
            $location = $this->modx->getObject('foodLocation', array('id' => $locationID));
            $result = array();

            // Don't create location without coordinates or GeoJSON data
            $exit = true;
            foreach ($properties as $key => $value) {
                if (strpos($key, 'Location_') !== false && !empty($value)) {
                    $exit = false;
                }
            }
            if ($exit) return $result;

            if (!is_object($location)) {
                $location = $this->modx->newObject('foodLocation', array(
                    'createdon' => time(),
                    'createdby' => $this->modx->user->get('id'),
                ));
                $location->save();

                // Set location ID in object
                $object->set('location_id', $location->get('id'));
                $object->save();
            }

            if ($location) {
                $location->fromArray($properties,'Location_');
                $this->resetNULL($location,$properties,'Location_');

                // Reset empty GeoJSON field to NULL
                if (!$properties['Location_geojson']) {
                    $location->set('geojson', NULL);
                }

                if (!$location->save()) {
                    $result = array('error' => 'Could not save location.');
                }
            }

            return $result;
        }
    }

    // Create and edit addresses inside referencing MIGXdb configs
    public function saveAddress($object, $properties, $addressID) {
        if (is_object($object)) {
            $address = $this->modx->getObject('foodAddress', array('id' => $addressID));
            $result = array();

            // Don't create address if there's no address data
            $exit = true;
            foreach ($properties as $key => $value) {
                if (strpos($key, 'Address_') !== false && !empty($value)) {
                    $exit = false;
                }
            }
            if ($exit) return $result;

            if (!is_object($address)) {
                $address = $this->modx->newObject('foodAddress', array(
                    'createdon' => time(),
                    'createdby' => $this->modx->user->get('id'),
                ));
                $address->save();

                // Set address ID in object
                $object->set('address_id', $address->get('id'));
                $object->save();
            }

            if ($address) {
                $address->fromArray($properties,'Address_');
                $this->resetNULL($address,$properties,'Address_');

                if (!$address->save()) {
                    $result = array('error' => 'Could not save address.');
                }
            }

            return $result;
        }
    }

    // Create and edit food users
    public function saveContact($object, $properties, $contactID) {
        if (is_object($object)) {
            $user = $this->modx->getObject('foodUser', array('id' => $contactID));
            $result = array();

            // Don't do anything if existing user selection is being changed
            if ($properties['contact_id'] != $properties['UserData_internalKey']) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, '[FoodBrain] Not touching!!');
                return '';
            }

            // Detect users that have accidentally been changed to modUser
            if (!is_object($user) && $this->modx->getObject('modUser', array('id' => $contactID))) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, '[FoodBrain] Contact ' . $contactID . ' is still a modUser!');
                return array('error' => 'Contact is still modUser!');
            }

            // Create new food user if needed
            if (!is_object($user)) {
                $user = $this->modx->newObject('foodUser');
                $userData = $this->modx->newObject('modUserProfile');

                // Generate username
                $this->generateUserName($properties,$user);
                $user->set('active', 0);
                $user->set('primary_group', $this->modx->getOption('foodbrain.usergroup_generated'));
                $user->save();

                // Create user profile
                $userData->set('internalKey', $user->get('id'));
                $userData->save();

                // Create extended user profile (if object isn't one already)
                if ($properties['classname'] != 'foodUserData') {
                    $foodUserData = $this->modx->newObject('foodUserData');
                    $foodUserData->set('user_id', $user->get('id'));
                    $foodUserData->save();
                }

                // Join generated user group
                // @todo: IDs don't seem to work here.. MODX bug?
                //$user->joinGroup('Generated Users','Member');

                // Set contact / user ID in object
                $object->set('user_id', $user->get('id'));
                $object->set('contact_id', $user->get('id'));
                $object->save();
            }

            // Contacts can only be edited by the creator of the MIGXdb element
            // @todo: Can't update the selector now, so it needs admin override
            //if ($this->modx->getAuthenticatedUser() != $object->get('createdby')) {
            //    $this->modx->log(modX::LOG_LEVEL_ERROR, '[FoodBrain] You\'re trying to save a contact that you do not maintain: ' . $user->get('username'));
            //    $result = array('error' => 'You can\'t save this item, because you\'re not the maintainer.');
            //}

            // By now we have a user object
            $userData = $user->getOne('Profile');

            //$this->modx->log(modX::LOG_LEVEL_ERROR, print_r($userData->toArray(), 1));
            //$this->modx->log(modX::LOG_LEVEL_ERROR, print_r($contactData->toArray(), 1));

            // Write user data
            $user->fromArray($properties,'Contact_');
            $userData->fromArray($properties,'UserData_');

            // Write foodUser data if object isn't foodUserData already
            if ($properties['classname'] != 'foodUserData') {
                $foodUserData = $user->getOne('ContactData');

                if (!is_object($foodUserData)) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, '[FoodBrain] No foodUserData!');
                    $foodUserData = $this->modx->newObject('foodUserData');
                    $foodUserData->set('user_id', $user->get('id'));
                    $foodUserData->save();
                }

                $foodUserData->fromArray($properties,'ContactData_');
                $this->resetNULL($foodUserData,$properties,'ContactData_');
            }

            // Replace auto-generated names and set fullname
            $firstName = $properties['ContactData_firstname'] ?? $properties['firstname'];
            $middleName = $properties['ContactData_middlename'] ?? $properties['middlename'];
            $lastName = $properties['ContactData_lastname'] ?? $properties['lastname'];
            $fullName = $firstName . ' ' . $middleName . ' ' . $lastName;

            if ($firstName && $lastName) {
                if (strpos($user->get('username'),'fooduser') !== false) {
                    $this->generateUserName($properties,$user);
                }
            }
            if (!$userData->get('fullname') || $userData->get('fullname') != $fullName) {
                $userData->set('fullname', $fullName);
                $userData->save();
            }

            // Reset NULL values
            $this->resetNULL($object,$properties);
            $this->resetNULL($user,$properties,'Contact_');
            $this->resetNULL($user,$properties,'User_');
            $this->resetNULL($userData,$properties,'UserData_');

            if (!$user->save()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, '[FoodBrain] Could not save contact: ' .  $user->get('username'));
                $result = array('error' => 'Could not save contact.');
            }

            return $result;
        }
    }

    // Generate unique username in MIGXdb grids
    private function generateUserName ($properties, $user) {
        $firstName = $properties['ContactData_firstname'] ?? $properties['firstname'];
        $lastName = $properties['ContactData_lastname'] ?? $properties['lastname'];

        $userName = strtolower($firstName . $lastName);
        $userName = preg_replace('/[^.A-Za-z0-9 _-]/', '', $userName); // strip non-alphanumeric characters
        $userName = str_replace(' ', '', $userName); // remove whitespace

        // Add incrementing suffix to duplicate names
        $idx = 1;
        while ($userName && $this->modx->getObject('foodUser', array('username' => $userName))) {
            $userName = preg_replace('/[^a-z]/', '', $userName);
            $userName = $userName . $idx++;
        }
        if ($userName) {
            $user->set('username', $userName);
        } else {
            $user->set('username', 'user' . time()); // temporary name
            $user->save(); // generate ID
            $user->set('username', 'fooduser' . $user->get('id'));
        }
    }

    // Validate JSON
    public function validateJSON ($json) {
        $parser = new JsonParser();

        // Result will be empty on success
        $validateOutput = $parser->lint($json);

        if ($validateOutput) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[FoodBrain] ' . $validateOutput);
            return array('error' => 'JSON not valid. See error log for details.');
        }

        return true;
    }

    /**
     * Pick random location within a given radius
     *
     * Given a $center (latitude,longitude) coordinates and a distance $radius
     * (kilometers), returns a random point (latitude,longitude) which is within
     * $radius kilometers of $center.
     *
     * @param  array $center Numeric array of floats. First element is
     *                       latitude, second is longitude.
     * @param  float $radius The radius (in kilometers).
     * @return array         Array of floats (lat/lng).
     *
     * @link   https://stackoverflow.com/a/21646258
     */
    public function obfuscateCoordinates (array $center, float $radius): array
    {
        $radius_earth = 6371; //kilometers

        // Pick random distance within $distance
        $distance = lcg_value() * $radius;

        // Convert degrees to radians
        $center_rads = array_map('deg2rad', $center);

        // First suppose our point is the north pole.
        // Find a random point $distance kilometers away.
        $lat_rads = (pi()/2) - $distance/$radius_earth;
        $lng_rads = lcg_value() * 2 * pi();

        // ($lat_rads,$lng_rads) is a point on the circle which is $distance
        // kilometers from the north pole. Convert to Cartesian.
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);

        // Rotate the sphere so that the north pole is now at $center.
        // Rotate in x axis by $rot = (pi()/2) - $center_rads[0].
        $rot = (pi() / 2) - $center_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);

        // Rotate in z axis by $rot = $center_rads[1]
        $rot = $center_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;

        // Finally convert this point to polar coordinates
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);

        return array_map('rad2deg', array(
            'lat' => $lat_rads,
            'lng' => $lng_rads
        ));
    }
}
