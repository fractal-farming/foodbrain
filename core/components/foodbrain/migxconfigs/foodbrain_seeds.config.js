{
    "formtabs": [
        {
            "MIGX_id": 1,
            "caption": "Properties",
            "print_before_tabs": "0",
            "pos": 1,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "taxonomy_id",
                    "caption": "Taxonomy",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectFoodOption@Taxonomy",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "variety",
                    "caption": "Variety",
                    "description": "Maybe there are still variations present within the selected species. Hurray for biodiversity! If you have a Durian seed for example, the variety might be Puyat, Arancillio, D101 or something similar.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "quantity",
                    "caption": "Quantity",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "quantity_unit",
                    "caption": "Quantity unit of measure",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectInputOption@UnitOfMeasure",
                    "default": "g",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "date_collected",
                    "caption": "Collection date",
                    "description": "This means: the date the seeds were collected from the flowers, or found on the forest floor.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": {
                        "hideTime": true
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "date_expired",
                    "caption": "Expiration date",
                    "description": "This means: the estimated shelf life of the seed.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": {
                        "hideTime": true
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "description",
                    "caption": "Description",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "richtext",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdon",
                    "caption": "Date",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdby",
                    "caption": "Steward",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "published",
                    "caption": "Public",
                    "description": "When set to 'Yes', this plant will be visible to the outside world. When set to 'No', only registered members can view this item.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "Yes==1||No==",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "deleted",
                    "caption": "deleted",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 2,
            "caption": "Instructions",
            "print_before_tabs": "0",
            "pos": 2,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "instructions",
                    "caption": "Instructions",
                    "description": "Tips and tricks for propagating and sowing this seed.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "richtext",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 3,
            "caption": "Source",
            "print_before_tabs": "0",
            "pos": 3,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "source_plant_id",
                    "caption": "Plant",
                    "description": "Select the parent plant that gifted you this seed. This list shows all individual plants in the database that were added by you.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectSeedSourcePlant",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "source_id",
                    "caption": "External",
                    "description": "Select the person or place that you got this seed from. Only needed if you didn't select a parent plant above. If the source is not included in the dropdown list, you can add it under the Manage Sources tab on the right.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectFoodOption@SourceSeed",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 5,
            "caption": "Manage sources",
            "print_before_tabs": "0",
            "pos": 5,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "sources",
                    "caption": "Sources",
                    "description": "Here you can maintain the list of potential suppliers. Note: these are all suppliers in the system (that you have access to), they're not yet connected to this seed. Once you've added a source here, please reopen this modal to make it available in the External Source dropdown.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "foodbrain_sources:foodbrain",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 8,
            "caption": "Cover image",
            "print_before_tabs": "0",
            "pos": 8,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "img_portrait",
                    "caption": "Portrait image",
                    "description": "This image has a 3:4 aspect ratio. Best suited for tall things, like trees.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "300",
                        "targetHeight": "400",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100006
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100006
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100006
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_landscape",
                    "caption": "Landscape image",
                    "description": "This image has a 4:3 aspect ratio. Best suited for wide things, like forests.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "500",
                        "targetHeight": "375",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100006
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100006
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100006
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_square",
                    "caption": "Square image",
                    "description": "This image has a 1:1 aspect ratio. Best suited for, well.. square things, like fruits.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "300",
                        "targetHeight": "300",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100006
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100006
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100006
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        }
    ],
    "contextmenus": "update||duplicate||recall_remove_delete",
    "actionbuttons": "addItem||toggletrash",
    "columnbuttons": "",
    "extended": {
        "migx_add": "Add seed",
        "disable_add_item": "",
        "add_items_directly": "",
        "formcaption": "",
        "update_win_title": "Edit seed",
        "win_id": "seed",
        "maxRecords": "",
        "addNewItemAt": "top",
        "media_source_id": "",
        "multiple_formtabs": "",
        "multiple_formtabs_label": "",
        "multiple_formtabs_field": "",
        "multiple_formtabs_optionstext": "",
        "multiple_formtabs_optionsvalue": "",
        "actionbuttonsperrow": 4,
        "winbuttonslist": "",
        "extrahandlers": "this.handleColumnSwitch",
        "filtersperrow": 4,
        "packageName": "foodbrain",
        "classname": "foodSeed",
        "task": "",
        "getlistsort": "id",
        "getlistsortdir": "DESC",
        "sortconfig": "",
        "gridpagesize": 30,
        "use_custom_prefix": "0",
        "prefix": "",
        "grid": "",
        "gridload_mode": 1,
        "check_resid": 1,
        "check_resid_TV": "",
        "join_alias": "",
        "has_jointable": "yes",
        "getlistwhere": "",
        "joins": [
            {
                "alias": "Taxonomy"
            },{
                "alias": "Source"
            },{
                "alias": "Plants"
            }
        ],
        "hooksnippets": {
            "aftersave": "migxResetNULL"
        },
        "cmpmaincaption": "SeedBrain",
        "cmptabcaption": "Seeds",
        "cmptabdescription": "A list of your collected seeds.",
        "cmptabcontroller": "",
        "winbuttons": "",
        "onsubmitsuccess": "",
        "submitparams": ""
    },
    "columns": [
        {
            "MIGX_id": "",
            "header": "ID",
            "dataIndex": "id",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Image",
            "dataIndex": "img_square",
            "width": 20,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "<img src='[[ImagePlus? &value=`[[+img_square:replace=`\"source\": 100006==\"source\": 1`:replace=`\"src\": \"==\"src\": \"/uploads/img/seed/[[+id]]/`]]`]]' style='max-width:100%;height:auto;'>",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Name",
            "dataIndex": "Taxonomy_name",
            "width": 50,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+Taxonomy_name:empty=`Unknown <i class=\"icon icon-fw icon-question-circle\" title=\"Needs ID\"></i>`]] [[+Taxonomy_name_local:prepend=`/ `]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Variety",
            "dataIndex": "variety",
            "width": 50,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": "this.textEditor"
        },
        {
            "MIGX_id": "",
            "header": "Quantity",
            "dataIndex": "quantity",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+quantity:append=` [[+quantity_unit]]`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Collected",
            "dataIndex": "date_collected",
            "width": 30,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+date_collected:date=`%d-%m-%Y`:ago]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Source",
            "dataIndex": "Source_id",
            "width": 50,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[migxLoopCollection:empty=`[[+Source_name:empty=`Unknown`]]`? &packageName=`foodbrain` &classname=`foodSpecies` &where=`[{\"id\":\"[[+Plant_taxonomy_id]]\"},{\"deleted:=\":0}]` &tpl=`@CODE:[[+name]]`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Public",
            "dataIndex": "published",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderSwitchStatusOptions",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": [
                {
                    "MIGX_id": 1,
                    "name": "No",
                    "use_as_fallback": 1,
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                },
                {
                    "MIGX_id": 2,
                    "name": "Yes",
                    "use_as_fallback": "",
                    "value": 1,
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/check-square-o.svg"
                },
                {
                    "MIGX_id": 3,
                    "name": "No",
                    "use_as_fallback": "",
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                }
            ],
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "deleted",
            "dataIndex": "deleted",
            "width": "",
            "sortable": "false",
            "show_in_grid": "0",
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        }
    ],
    "category": ""
}