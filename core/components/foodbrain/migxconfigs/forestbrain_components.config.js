{
    "formtabs": [
        {
            "MIGX_id": 1,
            "caption": "Properties",
            "print_before_tabs": "0",
            "pos": 1,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Forest_id",
                    "caption": "Forest ID",
                    "description": "This points to the forest resource.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Resource_id",
                    "caption": "Resource ID",
                    "description": "This resource is extended with data listed here.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Resource_pagetitle",
                    "caption": "Component",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "zone",
                    "caption": "Zone",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": {
                        "columns": "20"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "1||2||3||4||5",
                    "default": null,
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "type",
                    "caption": "Type",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "false"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectInputOption@ComponentType",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "stage",
                    "caption": "Stage",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "false"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectInputOption@ComponentStage",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Resource_introtext",
                    "caption": "Description",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "richtext",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdon",
                    "caption": "Date",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdby",
                    "caption": "Steward",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Resource_published",
                    "caption": "Public",
                    "description": "When set to 'Yes', this component will be visible to the outside world. When set to 'No', only registered members can view this item.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "Yes==1||No==0",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "deleted",
                    "caption": "deleted",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 2,
            "caption": "Inputs",
            "print_before_tabs": "0",
            "pos": 2,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "inputs",
                    "caption": "Inputs",
                    "description": "A list of inputs (internal or external) for this component.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_inputs:foodbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 3,
            "caption": "Outputs",
            "print_before_tabs": "0",
            "pos": 3,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "outputs",
                    "caption": "Outputs",
                    "description": "Everything this component produces (including \"waste\" products). These outputs can be used by other components as input.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_outputs:foodbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 4,
            "caption": "Location",
            "print_before_tabs": "0",
            "pos": 4,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "location_id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_lat",
                    "caption": "Latitude",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": {
                        "minValue": "-90",
                        "maxValue": "90",
                        "allowDecimals": "Yes",
                        "allowNegative": "Yes",
                        "decimalPrecision": "6",
                        "decimalSeparator": "."
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_lng",
                    "caption": "Longitude",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": {
                        "minValue": "-180",
                        "maxValue": "180",
                        "allowDecimals": "Yes",
                        "allowNegative": "Yes",
                        "decimalPrecision": "6",
                        "decimalSeparator": "."
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_elevation",
                    "caption": "Elevation",
                    "description": "Land height above sea level, in meters.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": {
                        "allowDecimals": "Yes",
                        "allowNegative": "Yes",
                        "decimalPrecision": "2",
                        "decimalSeparator": ","
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_radius",
                    "caption": "Randomize",
                    "description": "Protect sensitive locations by reducing the accuracy of the map marker. The selected value determines the radius around the exact location (in km), inside which a random point is chosen. The higher the value, the more inaccurate the marker position. And 0 means: use exact location.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": {
                        "columns": "20"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectFibonacci@LocationRadius",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_geojson",
                    "caption": "Features",
                    "description": "A GeoJSON object for displaying slightly more complex spatial data, such as a polygon for defining an area or a circle for specifying the radius of a tree. You can group multiple GeoJSON features together in a FeatureCollection, allowing you to add a variety of objects. To make it easier to create and edit this data, you can copy/paste the code in an online editor like https://geojson.io/.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "textarea",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 5,
            "caption": "Cover image",
            "print_before_tabs": "0",
            "pos": 5,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "img_portrait",
                    "caption": "Portrait image",
                    "description": "This image has a 3:4 aspect ratio. Best suited for tall things, like trees.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "300",
                        "targetHeight": "400",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100008
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_landscape",
                    "caption": "Landscape image",
                    "description": "This image has a 4:3 aspect ratio. Best suited for landscapy things, like forests.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "500",
                        "targetHeight": "375",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100008
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_square",
                    "caption": "Square image",
                    "description": "This image has a 1:1 aspect ratio. Best suited for, well.. square things, like fruits.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "300",
                        "targetHeight": "300",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100008
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_wide",
                    "caption": "Wide image",
                    "description": "This image has a 16:9 aspect ratio, for slightly wider landscapes.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "800",
                        "targetHeight": "450",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100008
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_pano",
                    "caption": "Panorama image",
                    "description": "This image has a 21:9 aspect ratio, perfect for use in page header.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "1300",
                        "targetRatio": "2.33",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100008
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_free",
                    "caption": "Free image",
                    "description": "This image doesn't have a fixed aspect ratio or any minimum dimensions. Use it as you see fit. Cropping is optional. NB! Be aware that images will probably have different sizes in overviews!",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100008
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100008
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 8,
            "caption": "Purpose",
            "print_before_tabs": "0",
            "pos": 8,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "requirements",
                    "caption": "Why is it needed?",
                    "description": "You can link this component to a requirement you created earlier (on your Forest Properties page). If the component addresses multiple needs, feel free to link more than one requirement.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_req_comp:foodbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 13,
            "caption": "Manage imports",
            "print_before_tabs": "0",
            "pos": 13,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "imports",
                    "caption": "Available resources",
                    "description": "A list of external inputs that you are able to access.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_imports:foodbrain",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        }
    ],
    "contextmenus": "update||duplicate||recall_remove_delete",
    "actionbuttons": "addItem||toggletrash",
    "columnbuttons": "",
    "extended": {
        "migx_add": "Add component",
        "disable_add_item": "",
        "add_items_directly": "",
        "formcaption": "",
        "update_win_title": "Edit component",
        "win_id": "component",
        "maxRecords": "",
        "addNewItemAt": "top",
        "media_source_id": "",
        "multiple_formtabs": "",
        "multiple_formtabs_label": "",
        "multiple_formtabs_field": "",
        "multiple_formtabs_optionstext": "",
        "multiple_formtabs_optionsvalue": "",
        "actionbuttonsperrow": 4,
        "winbuttonslist": "",
        "extrahandlers": "this.handleColumnSwitch",
        "filtersperrow": 4,
        "packageName": "foodbrain",
        "classname": "foodForestComponent",
        "task": "",
        "getlistsort": "id",
        "getlistsortdir": "DESC",
        "sortconfig": "",
        "gridpagesize": 30,
        "use_custom_prefix": "0",
        "prefix": "",
        "grid": "",
        "gridload_mode": 1,
        "check_resid": 0,
        "check_resid_TV": "",
        "join_alias": "",
        "has_jointable": "yes",
        "getlistwhere": "{\"forest_id\": \"[[+resource_id]]\"}",
        "joins": [
            {
                "alias": "Forest"
            },{
                "alias": "Resource"
            },{
                "alias": "Location"
            }
        ],
        "hooksnippets": {
            "beforesave": "migxVerifyData",
            "aftersave": "migxSaveComponent"
        },
        "cmpmaincaption": "",
        "cmptabcaption": "",
        "cmptabdescription": "",
        "cmptabcontroller": "",
        "winbuttons": "",
        "onsubmitsuccess": "",
        "submitparams": ""
    },
    "columns": [
        {
            "MIGX_id": "",
            "header": "ID",
            "dataIndex": "id",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Image",
            "dataIndex": "img_portrait",
            "width": 20,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "<img src='[[ImagePlus? &value=`[[+img_landscape:replace=`\"source\": 100008==\"source\": 1`:replace=`\"src\": \"==\"src\": \"/uploads/img/forest/[[+forest_id]]/components/`]]`]]' style='max-width:100%;height:auto;'>",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Component",
            "dataIndex": "Resource_pagetitle",
            "width": 50,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "<a href=\"?a=resource/update&id=[[+Resource_id]]\"><i class=\"icon icon-fw icon-edit\" title=\"Edit resource\"></i></a> [[+Resource_pagetitle]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Zone",
            "dataIndex": "zone",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": "this.textEditor"
        },
        {
            "MIGX_id": "",
            "header": "Type",
            "dataIndex": "type",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+type:ucfirst]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Stage",
            "dataIndex": "stage",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+stage:ucfirst]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Steward",
            "dataIndex": "createdby",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+createdby:userinfo=`username`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Public",
            "dataIndex": "Resource_published",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderSwitchStatusOptions",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": [
                {
                    "MIGX_id": 1,
                    "name": "No",
                    "use_as_fallback": 1,
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                },
                {
                    "MIGX_id": 2,
                    "name": "Yes",
                    "use_as_fallback": "",
                    "value": 1,
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/check-square-o.svg"
                },
                {
                    "MIGX_id": 3,
                    "name": "No",
                    "use_as_fallback": "",
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                }
            ],
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "deleted",
            "dataIndex": "deleted",
            "width": "",
            "sortable": "false",
            "show_in_grid": "0",
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        }
    ],
    "category": ""
}