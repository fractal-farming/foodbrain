{
    "formtabs": [
        {
            "MIGX_id": 1,
            "caption": "Species",
            "print_before_tabs": "0",
            "pos": 1,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Resource_id",
                    "caption": "Forest ID",
                    "description": "This points to the forest resource.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "taxonomy_id",
                    "caption": "Taxonomy",
                    "description": "Select which species this plant belongs to. If the species is not included in the dropdown list, you can add it under the 'Manage species' tab on the right.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectFoodOption@Taxonomy",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "variety",
                    "caption": "Variety",
                    "description": "Maybe there are variations within the selected species. Hurray for biodiversity! If you have a Durian tree for example, the variety might be Puyat, Arancillio, D101 or something similar.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "description",
                    "caption": "Description",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "richtext",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdon",
                    "caption": "Date",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdby",
                    "caption": "Steward",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "published",
                    "caption": "Public",
                    "description": "When set to 'Yes', this plant will be visible to the outside world. When set to 'No', only registered members can view this item.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "Yes==1||No==",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "deleted",
                    "caption": "deleted",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 2,
            "caption": "Individual plants",
            "print_before_tabs": "0",
            "pos": 2,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "plants",
                    "caption": "Plants",
                    "description": "All known physical locations where this plant is growing.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_plant_items:foodbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 3,
            "caption": "Inputs",
            "print_before_tabs": "0",
            "pos": 3,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "inputs",
                    "caption": "Inputs",
                    "description": "A list of inputs (internal or external) that you feed this plant.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_inputs:foodbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 5,
            "caption": "Outputs",
            "print_before_tabs": "0",
            "pos": 5,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "outputs",
                    "caption": "Outputs",
                    "description": "Everything this plant produces (from leaves to lumber). These outputs can be used by other components as input.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_outputs:foodbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 8,
            "caption": "Cover image",
            "print_before_tabs": "0",
            "pos": 8,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "img_portrait",
                    "caption": "Portrait image",
                    "description": "This image has a 3:4 aspect ratio. Best suited for tall things, like trees.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "300",
                        "targetHeight": "400",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100002
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100002
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100002
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_landscape",
                    "caption": "Landscape image",
                    "description": "This image has a 4:3 aspect ratio. Best suited for wide things, like forests.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "500",
                        "targetHeight": "375",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100002
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100002
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100002
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "img_square",
                    "caption": "Square image",
                    "description": "This image has a 1:1 aspect ratio. Best suited for, well.. square things, like fruits.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "imageplus",
                    "validation": "",
                    "configs": {
                        "targetWidth": "300",
                        "targetHeight": "300",
                        "allowAltTag": "true",
                        "allowCredits": "true"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": [
                        {
                            "MIGX_id": 1,
                            "context": "mgr",
                            "sourceid": 100002
                        },
                        {
                            "MIGX_id": 2,
                            "context": "web",
                            "sourceid": 100002
                        },
                        {
                            "MIGX_id": 3,
                            "context": "forestbrain",
                            "sourceid": 100002
                        }
                    ],
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 13,
            "caption": "Manage species",
            "print_before_tabs": "0",
            "pos": 13,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "species",
                    "caption": "Species",
                    "description": "Here you can maintain the list of plant species available in the system. Note: they're not yet connected to this plant. Once you've added a species here, please reopen this modal to make it available in the Taxonomy selector.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "foodbrain_taxonomy:foodbrain",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 21,
            "caption": "Manage imports",
            "print_before_tabs": "0",
            "pos": 21,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "imports",
                    "caption": "Available resources",
                    "description": "A list of external inputs that you are able to access.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "forestbrain_imports:foodbrain",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        }
    ],
    "contextmenus": "update||duplicate||recall_remove_delete",
    "actionbuttons": "addItem||toggletrash",
    "columnbuttons": "",
    "extended": {
        "migx_add": "Add plant",
        "disable_add_item": "",
        "add_items_directly": "",
        "formcaption": "",
        "update_win_title": "Edit plant",
        "win_id": "plant",
        "maxRecords": "",
        "addNewItemAt": "top",
        "media_source_id": "",
        "multiple_formtabs": "",
        "multiple_formtabs_label": "",
        "multiple_formtabs_field": "",
        "multiple_formtabs_optionstext": "",
        "multiple_formtabs_optionsvalue": "",
        "actionbuttonsperrow": 4,
        "winbuttonslist": "",
        "extrahandlers": "this.handleColumnSwitch",
        "filtersperrow": 4,
        "packageName": "foodbrain",
        "classname": "foodForestPlant",
        "task": "",
        "getlistsort": "id",
        "getlistsortdir": "DESC",
        "sortconfig": "",
        "gridpagesize": 30,
        "use_custom_prefix": "0",
        "prefix": "",
        "grid": "",
        "gridload_mode": 1,
        "check_resid": 1,
        "check_resid_TV": "",
        "join_alias": "",
        "has_jointable": "yes",
        "getlistwhere": "",
        "joins": [
            {
                "alias": "Forest"
            },{
                "alias": "Resource"
            },{
                "alias": "Taxonomy"
            }
        ],
        "hooksnippets": {
            "aftersave": "migxResetNULL"
        },
        "cmpmaincaption": "ForestBrain",
        "cmptabcaption": "Plants",
        "cmptabdescription": "An overview of plant species that were identified in each forest.",
        "cmptabcontroller": "",
        "winbuttons": "",
        "onsubmitsuccess": "",
        "submitparams": ""
    },
    "columns": [
        {
            "MIGX_id": "",
            "header": "ID",
            "dataIndex": "id",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Forest",
            "dataIndex": "Forest_resource_id",
            "width": 10,
            "sortable": true,
            "show_in_grid": 0,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Image",
            "dataIndex": "img_portrait",
            "width": 20,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "<img src='[[ImagePlus? &value=`[[+img_portrait:replace=`\"source\": 100002==\"source\": 1`:replace=`\"src\": \"==\"src\": \"/uploads/img/forest/[[+Forest_resource_id]]/plants/`]]`]]' style='max-width:100%;height:auto;'>",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Name",
            "dataIndex": "Taxonomy_name",
            "width": 30,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+Taxonomy_name:empty=`Unknown <i class=\"icon icon-fw icon-question-circle\" title=\"Needs ID\"></i>`]] [[+Taxonomy_name_local:prepend=`/ `]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Description",
            "dataIndex": "description",
            "width": 50,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": "this.textEditor"
        },
        {
            "MIGX_id": "",
            "header": "Plants",
            "dataIndex": "plants",
            "width": 30,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[migxLoopCollection? &packageName=`foodbrain` &classname=`foodForestPlantItem` &where=`[{\"plant_id\":\"[[+id]]\"},{\"deleted:=\":0}]` &tpl=`migxForestPlantItemRow` &outputSeparator=`<br>`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Forest",
            "dataIndex": "Resource_pagetitle",
            "width": 30,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "<a href=\"?a=resource/update&id=[[+Resource_id]]\"><i class=\"icon icon-fw icon-edit\" title=\"Edit forest\"></i></a> [[+Resource_pagetitle]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Public",
            "dataIndex": "published",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderSwitchStatusOptions",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": [
                {
                    "MIGX_id": 1,
                    "name": "No",
                    "use_as_fallback": 1,
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                },
                {
                    "MIGX_id": 2,
                    "name": "Yes",
                    "use_as_fallback": "",
                    "value": 1,
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/check-square-o.svg"
                },
                {
                    "MIGX_id": 3,
                    "name": "No",
                    "use_as_fallback": "",
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                }
            ],
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "deleted",
            "dataIndex": "deleted",
            "width": "",
            "sortable": "false",
            "show_in_grid": "0",
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        }
    ],
    "category": ""
}