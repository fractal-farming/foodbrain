# Changelog for FoodBrain

## FoodBrain 0.5.3
Released on ...

New features:
- Add snippet to import data from Kobo [WIP]
- Add template for members pages [WIP]
- Add form for changing user password [WIP]
- Add login button for main menu [WIP]

Fixes and improvements:
- Fix form dropdown for selecting forest
- Set extended information for activated FoodUser

## FoodBrain 0.5.2
Released on January 22, 2022

New features:
- Add grid for editing contacts

Fixes and improvements:
- Don't create address or location if there's no data
- Replace hardcoded reference to map CB with system setting
- Add middle name to contact data

## FoodBrain 0.5.1
Released on December 10, 2021

New features:
- Add component table overview
- Add component overviews

Fixes and improvements:
- Control map base layers and default layers with CB setting
- Add search to Taxonomy grid
- Allow inputs and outputs to be connected to plants
- Don't display requirements from other forests in forest resource
- Move components grid to forest resource
- Tie component relations to object instead of resource
- Refer to maintainers as Stewards

## FoodBrain 0.5.0
Released on October 25, 2021

New features:
- Add forest table overview
- Add local forest template to improve sharing data across contexts
- Add ability to obfuscate coordinates
- Create separate database table for seeds
- Add forest features
- Add forest zones
- Dynamically load multiple map layers with a repeater CB
- Add forest components
- Add ForestBrain menu

Fixes and improvements:
- Compatibility fixes for Romanesco 1.0.0-beta8
- Add dropdown row tpl for selecting plant species in form
- Add resource field to edit maintainer (createdby)
- Add tab for managing species to 'Edit plant' modal
- Add variety field to plants
- Prevent save action in forest CMP on error
- Delete foodUser data if user is deleted
- Rename migxProcessDecimals to migxVerifyData
- Don't generate new user when changing a contact
- Update createdby values of related data when changing forest maintainer
- Activate contact information in forest properties
- Don't load individual plants grid when adding a new plant
- Add snippet to load forest boundaries layer on map
- Fix fatal error in forest map when plant item has no name
- Add terrain / satellite switch to map
- Move address data from locations to separate table

## FoodBrain 0.4.1
Released on July 20, 2020

New features:
- Add event detail template

Fixes and improvements:
- Display plant list with labels in forest template
- Add StuartXchange URL to taxonomy table
- Add resolver for syncing db tables after package update
- Improve layout of forest template

## FoodBrain 0.4.0
Released on May 4, 2020

New features:
- Add forest overviews
- Add image to food map popup
- Add food map toolbar

Fixes and improvements:
- Make container ID dynamic in forest map
- Add custom template elements for events
- Automatically arrange MIGX images in subfolders, by resource ID
- Move cover images to foodForest table

## FoodBrain 0.3.2
Released on January 1, 2020

Fixes and improvements:
- Only activate mousewheel zoom on focus in map container
- Show plant species in map popup
- Show buttons with website and facebook links in map popup
- Disable plant items and events on food forest map
- Add social media channels and website to foodForest table

## FoodBrain 0.3.1
Released on December 26, 2019

New features:
- Add LGU field to Location table

## FoodBrain 0.3.0
Released on December 25, 2019

New features:
- Add map view with forests, plant items and events
- Use Mapbox for loading tiles and styles
- Use Agenda extra for managing events

Fixes and improvements:
- Fix incorrect reference to forest resource ID in schema
- Don't load forests without coordinates in map
- Change location field for longitude to lng
- Add editedon and editedby fields to user-editable tables
- Don't rely on post values for retrieving data from MIGXdb objects
- Improve display of data in MIGX grids

## FoodBrain 0.2.0
Released on December 1, 2019

New features:
- Add foodUser and foodUserData objects
- Add Gitify configs for extracting dev and live data

Fixes and improvements
- Rename foodTaxonomy class to foodSpecies
- Remove MIGX_id and position values from configs
- Add species lifecycle to taxonomy properties
- Fix contact data not being saved when adding new source
- Add common snippet functions to FoodBrain class
- Improvements to foodSource config and saveContact function
- Add elevation field to Locations
- Integrate Location settings in Forest config
- Create hook snippets per object for MIGXdb configs

## FoodBrain 0.1.0
Released on November 7, 2019

Initial project setup:
- Add MIGXdb configs
- Add database schema
