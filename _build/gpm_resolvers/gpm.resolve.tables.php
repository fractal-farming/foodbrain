<?php
/**
 * Resolve creating db tables
 *
 * THIS RESOLVER IS AUTOMATICALLY GENERATED, NO CHANGES WILL APPLY
 *
 * @package foodbrain
 * @subpackage build
 *
 * @var mixed $object
 * @var modX $modx
 * @var array $options
 */

if ($object->xpdo) {
    $modx =& $object->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            $modelPath = $modx->getOption('foodbrain.core_path', null, $modx->getOption('core_path') . 'components/foodbrain/') . 'model/';
            
            $modx->addPackage('foodbrain', $modelPath, null);


            $manager = $modx->getManager();

            $manager->createObjectContainer('foodSpecies');
            $manager->createObjectContainer('foodSeed');
            $manager->createObjectContainer('foodForest');
            $manager->createObjectContainer('foodForestPlant');
            $manager->createObjectContainer('foodForestPlantItem');
            $manager->createObjectContainer('foodForestFeature');
            $manager->createObjectContainer('foodForestZone');
            $manager->createObjectContainer('foodForestRequirement');
            $manager->createObjectContainer('foodForestComponent');
            $manager->createObjectContainer('foodForestReqComp');
            $manager->createObjectContainer('foodForestInput');
            $manager->createObjectContainer('foodForestOutput');
            $manager->createObjectContainer('foodForestImport');
            $manager->createObjectContainer('foodForestImportSource');
            $manager->createObjectContainer('foodSource');
            $manager->createObjectContainer('foodLocation');
            $manager->createObjectContainer('foodAddress');
            $manager->createObjectContainer('foodUserData');

            break;
    }
}

return true;